<?xml version="1.0"?>
<TS>
  <context>
    <name>About</name>
    <message>
      <source>Done</source>
      <translation>Hotovo</translation>
    </message>
  </context>
  <context>
    <name>CaProperties</name>
    <message>
      <source>CA Properties</source>
      <translation>Vlastnosti CA</translation>
    </message>
    <message>
      <source>Use random Serial numbers</source>
      <translation>Použiť náhodné sériové čísla</translation>
    </message>
    <message>
      <source>Days until next CRL issuing</source>
      <translation>Dní do nasledujúceho vydania CRL</translation>
    </message>
    <message>
      <source>Default template</source>
      <translation>Predvolená šablóna</translation>
    </message>
    <message>
      <source>Next serial for signing</source>
      <translation>Sériové číslo ďalšieho podpisu</translation>
    </message>
  </context>
  <context>
    <name>CertDetail</name>
    <message>
      <source>Details of the Certificate</source>
      <translation>Podrobnosti certifikátu</translation>
    </message>
    <message>
      <source>S&amp;tatus</source>
      <translation>S&amp;tav</translation>
    </message>
    <message>
      <source>Serial</source>
      <translation>Sériové číslo</translation>
    </message>
    <message>
      <source>The serial number of the certificate</source>
      <translation>Sériové číslo certifikátu</translation>
    </message>
    <message>
      <source>The internal name of the certificate in the database</source>
      <translation>Interný názov certifikátu v databáze</translation>
    </message>
    <message>
      <source>Internal name</source>
      <translation>Interný názov</translation>
    </message>
    <message>
      <source>Signature algorithm</source>
      <translation>Algoritmus podpisu</translation>
    </message>
    <message>
      <source>Signature</source>
      <translation>Podpis</translation>
    </message>
    <message>
      <source>Key</source>
      <translation>Kľúč</translation>
    </message>
    <message>
      <source>Fingerprints</source>
      <translation>Odtlačky</translation>
    </message>
    <message>
      <source>MD5</source>
      <translation>MD5</translation>
    </message>
    <message>
      <source>An md5 hashsum of the certificate</source>
      <translation>Odtlačok certifikátu MD5</translation>
    </message>
    <message>
      <source>SHA1</source>
      <translation>SHA1</translation>
    </message>
    <message>
      <source>A SHA-1 hashsum of the certificate</source>
      <translation>Odtlačok certifikátu SHA-1</translation>
    </message>
    <message>
      <source>SHA256</source>
      <translation>SHA256</translation>
    </message>
    <message>
      <source>A SHA-256 hashsum of the certificate</source>
      <translation>Odtlačok certifikátu SHA-256</translation>
    </message>
    <message>
      <source>Validity</source>
      <translation>Platnosť</translation>
    </message>
    <message>
      <source>The time since the certificate is valid</source>
      <translation>Čas odkedy je certifikát platný</translation>
    </message>
    <message>
      <source>The time until the certificate is valid</source>
      <translation>Čas dokedy je certifikát platný</translation>
    </message>
    <message>
      <source>&amp;Subject</source>
      <translation>&amp;Predmet</translation>
    </message>
    <message>
      <source>&amp;Issuer</source>
      <translation>&amp;Vydavateľ</translation>
    </message>
    <message>
      <source>Attributes</source>
      <translation>Atribúty</translation>
    </message>
    <message>
      <source>&amp;Extensions</source>
      <translation>&amp;Rozšírenia</translation>
    </message>
    <message>
      <source>Show config</source>
      <translation>Zobraziť konfiguráciu</translation>
    </message>
    <message>
      <source>Show extensions</source>
      <translation>Zobraziť rozšírenia</translation>
    </message>
    <message>
      <source>Not available</source>
      <translation>Nedostupné</translation>
    </message>
    <message>
      <source>Signer unknown</source>
      <translation>Neznámy podpis</translation>
    </message>
    <message>
      <source>Self signed</source>
      <translation>Sebou podpísaný</translation>
    </message>
    <message>
      <source>Not trusted</source>
      <translation>Nedôveryhodný</translation>
    </message>
    <message>
      <source>Trusted</source>
      <translation>Dôveryhodný</translation>
    </message>
    <message>
      <source>Revoked: </source>
      <translation>Odvolaný: </translation>
    </message>
    <message>
      <source>Not valid</source>
      <translation>Neplatný</translation>
    </message>
    <message>
      <source>Valid</source>
      <translation>Platný</translation>
    </message>
    <message>
      <source>Details of the certificate signing request</source>
      <translation>Podrobnosti žiadosti o podpísanie certifikátu</translation>
    </message>
  </context>
  <context>
    <name>CertExtend</name>
    <message>
      <source>Certificate renewal</source>
      <translation>Obnovenie certifikátu</translation>
    </message>
    <message>
      <source>This will create a new certificate as a copy of the old one with a new serial number and adjusted validity values.</source>
      <translation>Týmto vytvoríte nový certifikát ako kópiu starého s novým sériovým číslom a novými hodnotami platnosti.</translation>
    </message>
    <message>
      <source>Validity</source>
      <translation>Platnosť</translation>
    </message>
    <message>
      <source>Not before</source>
      <translation>Nie pred</translation>
    </message>
    <message>
      <source>Not after</source>
      <translation>Nie po</translation>
    </message>
    <message>
      <source>Time range</source>
      <translation>Časový rozsah</translation>
    </message>
    <message>
      <source>Local time</source>
      <translation>Lokálny čas</translation>
    </message>
    <message>
      <source>Days</source>
      <translation>Dni</translation>
    </message>
    <message>
      <source>Months</source>
      <translation>Mesiace</translation>
    </message>
    <message>
      <source>Years</source>
      <translation>Roky</translation>
    </message>
    <message>
      <source>No well-defined expiration</source>
      <translation>Bez vypršania</translation>
    </message>
    <message>
      <source>Midnight</source>
      <translation>Polnoc</translation>
    </message>
    <message>
      <source>Apply</source>
      <translation>Použiť</translation>
    </message>
    <message>
      <source>Revoke old certificate</source>
      <translation>Odvolať starý certifikát</translation>
    </message>
    <message>
      <source>The certificate will be earlier valid than the signer. This is probably not what you want.</source>
      <translation>Certifikát bude platný skôr ako jeho vydavateľ. To asi nie je to, čo chcete.</translation>
    </message>
    <message>
      <source>Edit dates</source>
      <translation>Upraviť dátumy</translation>
    </message>
    <message>
      <source>Abort rollout</source>
      <translation>Prerušiť operáciu</translation>
    </message>
    <message>
      <source>Continue rollout</source>
      <translation>Pokračovať v operácii</translation>
    </message>
    <message>
      <source>Adjust date and continue</source>
      <translation>Opraviť dátum a pokračovať</translation>
    </message>
    <message>
      <source>The certificate will be longer valid than the signer. This is probably not what you want.</source>
      <translation>Certifikát bude platný dlhšie ako jeho vydavateľ. To asi nie je to, čo chcete.</translation>
    </message>
  </context>
  <context>
    <name>CertTreeView</name>
    <message>
      <source>Import PKCS#12</source>
      <translation>Importovať PKCS#12</translation>
    </message>
    <message>
      <source>Import from PKCS#7</source>
      <translation>Importovať z PKCS#7</translation>
    </message>
    <message>
      <source>Request</source>
      <translation>Žiadosť</translation>
    </message>
    <message>
      <source>Security token</source>
      <translation>Bezpečnostný token</translation>
    </message>
    <message>
      <source>Other token</source>
      <translation>Iný token</translation>
    </message>
    <message>
      <source>Similar Certificate</source>
      <translation>Podobný certifikát</translation>
    </message>
    <message>
      <source>Delete from Security token</source>
      <translation>Odstrániť z Bezpečnostného tokenu</translation>
    </message>
    <message>
      <source>CA</source>
      <translation>CA</translation>
    </message>
    <message>
      <source>Properties</source>
      <translation>Vlastnosti</translation>
    </message>
    <message>
      <source>Generate CRL</source>
      <translation>Generovať CRL</translation>
    </message>
    <message>
      <source>Manage revocations</source>
      <translation>Spravovať odvolania</translation>
    </message>
    <message>
      <source>Trust</source>
      <translation>Dôvera</translation>
    </message>
    <message>
      <source>Renewal</source>
      <translation>Obnoviť</translation>
    </message>
    <message>
      <source>Revoke</source>
      <translation>Odvolať</translation>
    </message>
    <message>
      <source>Unrevoke</source>
      <translation>Zrušiť odvolanie</translation>
    </message>
  </context>
  <context>
    <name>ClickLabel</name>
    <message>
      <source>Double click for details</source>
      <translation>Dvojklikom podrobnosti</translation>
    </message>
  </context>
  <context>
    <name>CrlDetail</name>
    <message>
      <source>Details of the Revocation list</source>
      <translation>Podrobnosti CRL</translation>
    </message>
    <message>
      <source>&amp;Status</source>
      <translation>&amp;Stav</translation>
    </message>
    <message>
      <source>Version</source>
      <translation>Verzia</translation>
    </message>
    <message>
      <source>Signature</source>
      <translation>Podpis</translation>
    </message>
    <message>
      <source>Signed by</source>
      <translation>Podpísal</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>Názov</translation>
    </message>
    <message>
      <source>The internal name of the CRL in the database</source>
      <translation>Interný názov CRL v databáze</translation>
    </message>
    <message>
      <source>issuing dates</source>
      <translation>Dátumy vydania</translation>
    </message>
    <message>
      <source>Next update</source>
      <translation>Nasled. aktualizácia</translation>
    </message>
    <message>
      <source>Last update</source>
      <translation>Posledná aktualizácia</translation>
    </message>
    <message>
      <source>&amp;Issuer</source>
      <translation>&amp;Vydavateľ</translation>
    </message>
    <message>
      <source>&amp;Extensions</source>
      <translation>&amp;Rozšírenia</translation>
    </message>
    <message>
      <source>&amp;Revocation list</source>
      <translation>Zoznam &amp;odvolaní</translation>
    </message>
    <message>
      <source>Failed</source>
      <translation>Zlyhalo</translation>
    </message>
    <message>
      <source>Unknown signer</source>
      <translation>Neznámy podpis</translation>
    </message>
    <message>
      <source>Verification not possible</source>
      <translation>Overenie nemožné</translation>
    </message>
  </context>
  <context>
    <name>ExportDialog</name>
    <message>
      <source>Name</source>
      <translation>Názov</translation>
    </message>
    <message>
      <source>The internal name of the CRL in the database</source>
      <translation>Interný názov CRL v databáze</translation>
    </message>
    <message>
      <source>...</source>
      <translation>...</translation>
    </message>
    <message>
      <source>Filename</source>
      <translation>Meno súboru</translation>
    </message>
    <message>
      <source>Export Format</source>
      <translation>Formát exportu</translation>
    </message>
    <message>
      <source>All files ( * )</source>
      <translation>Všetky súbory (*)</translation>
    </message>
    <message>
      <source>PEM Text format with headers</source>
      <translation>Textový formát PEM s hlavičkami</translation>
    </message>
    <message>
      <source>Concatenated list of all selected items in one PEM text file</source>
      <translation>Zlúčený zoznam zvolených položiek v jednom textovom súbore PEM</translation>
    </message>
    <message>
      <source>Concatenated text format of the complete certificate chain in one PEM file</source>
      <translation>Zlúčený textový formát úplnej reťaze certifikátu v jednom súbore PEM</translation>
    </message>
    <message>
      <source>Concatenated text format of all trusted certificates in one PEM file</source>
      <translation>Zlúčený textový formát všetkých dôveryhodných certifikátov v jednom súbore PEM</translation>
    </message>
    <message>
      <source>Concatenated text format of all certificates in one PEM file</source>
      <translation>Zlúčený textový formát všetkých certifikátov v jednom súbore PEM</translation>
    </message>
    <message>
      <source>Binary DER encoded file</source>
      <translation>Binárne DER kódovaný súbor</translation>
    </message>
    <message>
      <source>PKCS#7 encoded single certificate</source>
      <translation>PKCS#7 kódovaný jeden certifikát</translation>
    </message>
    <message>
      <source>PKCS#7 encoded complete certificate chain</source>
      <translation>PKCS#7 kódovaná úplná reťaz certifikátov</translation>
    </message>
    <message>
      <source>All trusted certificates encoded in one PKCS#7 file</source>
      <translation>Všetky dôveryhodné certifikáty kódované v jednom súbore PKCS#7</translation>
    </message>
    <message>
      <source>All selected certificates encoded in one PKCS#7 file</source>
      <translation>Všetky zvolené certifikáty kódované v jednom súbore PKCS#7</translation>
    </message>
    <message>
      <source>All certificates encoded in one PKCS#7 file</source>
      <translation>Všetky certifikáty kódované v jednom súbore PKCS#7</translation>
    </message>
    <message>
      <source>The certificate and the private key as encrypted PKCS#12 file</source>
      <translation>Certifikát a súkromný kľúč ako šifrovaný súbor PKCS#12</translation>
    </message>
    <message>
      <source>The complete certificate chain and the private key as encrypted PKCS#12 file</source>
      <translation>Úplná reťaz certifikátu a súkromný kľúč ako šifrovaný súbor PKCS#12</translation>
    </message>
    <message>
      <source>Concatenation of the certificate and the unencrypted private key in one PEM file</source>
      <translation>Spojenie certifikátu a nešifrovaného súkromného kľúča v jednom súbore PEM</translation>
    </message>
    <message>
      <source>Concatenation of the certificate and the encrypted private key in PKCS#8 format in one file</source>
      <translation>Spojenie certifikátu a šifrovaného súkromného kľúča v jednom súbore PKCS#8</translation>
    </message>
    <message>
      <source>Text format of the public key in one PEM file</source>
      <translation>Textový formát verejného kľúča v súbore PEM</translation>
    </message>
    <message>
      <source>Binary DER format of the public key</source>
      <translation>Binárny formát DER verejného kľúča</translation>
    </message>
    <message>
      <source>Unencrypted private key in text format</source>
      <translation>Nešifrovaný súkromný kľúč v textovom formáte</translation>
    </message>
    <message>
      <source>OpenSSL specific encrypted private key in text format</source>
      <translation>Šifrovaný súkromný kľúč v textovom formáte (špecifický pre OpenSSL)</translation>
    </message>
    <message>
      <source>Unencrypted private key in binary DER format</source>
      <translation>Nešifrovaný súkromný kľúč v binárnom formáte DER</translation>
    </message>
    <message>
      <source>Unencrypted private key in PKCS#8 text format</source>
      <translation>Nešifrovaný súkromný kľúč v textovom formáte PKCS#8</translation>
    </message>
    <message>
      <source>Encrypted private key in PKCS#8 text format</source>
      <translation>Šifrovaný súkromný kľúč v textovom formáte PKCS#8</translation>
    </message>
    <message>
      <source>The public key encoded in SSH2 format</source>
      <translation>Verejný kľúč kódovaný vo formáte SSH2</translation>
    </message>
    <message>
      <source>Certificate Index file</source>
      <translation>Súbor indexu certifikátov</translation>
    </message>
    <message>
      <source>The file: '%1' already exists!</source>
      <translation>Súbor: „%1” už existuje!</translation>
    </message>
    <message>
      <source>Overwrite</source>
      <translation>Prepísať</translation>
    </message>
    <message>
      <source>Do not overwrite</source>
      <translation>Neprepísať</translation>
    </message>
  </context>
  <context>
    <name>Help</name>
    <message>
      <source>&lt;&lt;</source>
      <translation>&lt;&lt;</translation>
    </message>
    <message>
      <source>&gt;&gt;</source>
      <translation>&gt;&gt;</translation>
    </message>
    <message>
      <source>&amp;Done</source>
      <translation>&amp;Hotovo</translation>
    </message>
  </context>
  <context>
    <name>ImportMulti</name>
    <message>
      <source>Import PKI Items</source>
      <translation>Importovať položky PKI</translation>
    </message>
    <message>
      <source>Import &amp;All</source>
      <translation>Importovať &amp;všetky</translation>
    </message>
    <message>
      <source>&amp;Import</source>
      <translation>&amp;Importovať</translation>
    </message>
    <message>
      <source>&amp;Done</source>
      <translation>&amp;Hotovo</translation>
    </message>
    <message>
      <source>&amp;Remove from list</source>
      <translation>Odstrániť zo zoznamu</translation>
    </message>
    <message>
      <source>Details</source>
      <translation>Podrobnosti</translation>
    </message>
    <message>
      <source>Delete from token</source>
      <translation>Odstrániť z tokenu</translation>
    </message>
    <message>
      <source>Rename on token</source>
      <translation>Premenovať token</translation>
    </message>
    <message>
      <source>
Name: %1
Model: %2
Serial: %3</source>
      <translation>
Názov: %1
Model: %2
Sér. č.: %3</translation>
    </message>
    <message>
      <source>Manage security token</source>
      <translation>Spravovať bezpečnostný token</translation>
    </message>
    <message>
      <source>Details of the item '%1' cannot be shown</source>
      <translation>Podrobnosti položky „%1” nemožno zobraziť</translation>
    </message>
    <message>
      <source>The type of the item '%1' is not recognized</source>
      <translation>Typ položky „%1” nebol rozpoznaný</translation>
    </message>
    <message>
      <source>The file '%1' did not contain PKI data</source>
      <translation>Súbor „%1” neobsahuje dáta PKI</translation>
    </message>
    <message>
      <source>The %1 files: '%2' did not contain PKI data</source>
      <translation>%1 súbory: „%2” neobsahuje dáta PKI</translation>
    </message>
  </context>
  <context>
    <name>KeyDetail</name>
    <message>
      <source>Name</source>
      <translation>Názov</translation>
    </message>
    <message>
      <source>The internal name of the key used by xca</source>
      <translation>Interný názov kľúča používaný v xca</translation>
    </message>
    <message>
      <source>Security token</source>
      <translation>Bezpečnostný token</translation>
    </message>
    <message>
      <source>Manufacturer</source>
      <translation>Výrobca</translation>
    </message>
    <message>
      <source>Serial</source>
      <translation>Sériové číslo</translation>
    </message>
    <message>
      <source>Key</source>
      <translation>Kľúč</translation>
    </message>
    <message>
      <source>Public Exponent</source>
      <translation>Verejný exponent</translation>
    </message>
    <message>
      <source>Keysize</source>
      <translation>Veľkosť kľúča</translation>
    </message>
    <message>
      <source>Private Exponent</source>
      <translation>Súkromný exponent</translation>
    </message>
    <message>
      <source>Modulus</source>
      <translation>Modulus</translation>
    </message>
    <message>
      <source>Details of the %1 key</source>
      <translation>Podrobnosti kľúča %1</translation>
    </message>
    <message>
      <source>Not available</source>
      <translation>Nedostupné</translation>
    </message>
    <message>
      <source>Token</source>
      <translation>Token</translation>
    </message>
    <message>
      <source>Security token ID:%1</source>
      <translation>Bezpečnostný token ID:%1</translation>
    </message>
    <message>
      <source>Available</source>
      <translation>Dostupný</translation>
    </message>
    <message>
      <source>Sub prime</source>
      <translation>Sub prime</translation>
    </message>
    <message>
      <source>Public key</source>
      <translation>Verejný kľúč</translation>
    </message>
    <message>
      <source>Private key</source>
      <translation>Súkromný kľúč</translation>
    </message>
    <message>
      <source>Curve name</source>
      <translation>Meno krivky</translation>
    </message>
    <message>
      <source>Unknown key</source>
      <translation>Neznámy kľúč</translation>
    </message>
  </context>
  <context>
    <name>KeyTreeView</name>
    <message>
      <source>Change password</source>
      <translation>Zmeniť heslo</translation>
    </message>
    <message>
      <source>Reset password</source>
      <translation>Odstrániť heslo</translation>
    </message>
    <message>
      <source>Change PIN</source>
      <translation>Zmeniť PIN</translation>
    </message>
    <message>
      <source>Init PIN with SO PIN (PUK)</source>
      <translation>Inicializovať PIN pomocou SO PIN (PUK)</translation>
    </message>
    <message>
      <source>Change SO PIN (PUK)</source>
      <translation>Zmeniť SO PIN (PUK)</translation>
    </message>
    <message>
      <source>Security token</source>
      <translation>Bezpečnostný token</translation>
    </message>
    <message>
      <source>This is not a token</source>
      <translation>Toto nie je token</translation>
    </message>
    <message>
      <source>Shall the original key '%1' be replaced by the key on the token?
This will delete the key '%1' and make it unexportable</source>
      <translation>Má byť pôvodný kľúč „%1” nahradený kľúčom na tokene?
Týmto bude odstránený kľúč „%1” a nebude ho možné exportovať</translation>
    </message>
  </context>
  <context>
    <name>MainWindow</name>
    <message>
      <source>Private Keys</source>
      <translation>Súkromné kľúče</translation>
    </message>
    <message>
      <source>&amp;New Key</source>
      <translation>&amp;Nový kľúč</translation>
    </message>
    <message>
      <source>&amp;Export</source>
      <translation>&amp;Exportovať</translation>
    </message>
    <message>
      <source>&amp;Import</source>
      <translation>&amp;Importovať</translation>
    </message>
    <message>
      <source>Import PFX (PKCS#12)</source>
      <translation>Importovať PFX (PKCS#12)</translation>
    </message>
    <message>
      <source>&amp;Show Details</source>
      <translation>&amp;Zobraziť podrobnosti</translation>
    </message>
    <message>
      <source>&amp;Delete</source>
      <translation>&amp;Odstrániť</translation>
    </message>
    <message>
      <source>Certificate signing requests</source>
      <translation>Žiadosti o podpísanie</translation>
    </message>
    <message>
      <source>&amp;New Request</source>
      <translation>&amp;Nová žiadosť</translation>
    </message>
    <message>
      <source>Certificates</source>
      <translation>Certifikáty</translation>
    </message>
    <message>
      <source>&amp;New Certificate</source>
      <translation>&amp;Nový certifikát</translation>
    </message>
    <message>
      <source>Import &amp;PKCS#12</source>
      <translation>Importovať &amp;PKCS#12</translation>
    </message>
    <message>
      <source>Import P&amp;KCS#7</source>
      <translation>Importovať P&amp;KCS#7</translation>
    </message>
    <message>
      <source>Plain View</source>
      <translation>Prosté zobrazenie</translation>
    </message>
    <message>
      <source>Templates</source>
      <translation>Šablóny</translation>
    </message>
    <message>
      <source>&amp;New Template</source>
      <translation>&amp;Nová šablóna</translation>
    </message>
    <message>
      <source>&amp;New CRL</source>
      <translation>&amp;Nový CRL</translation>
    </message>
    <message>
      <source>Ch&amp;ange Template</source>
      <translation>&amp;Zmeniť šablónu</translation>
    </message>
    <message>
      <source>Revocation lists</source>
      <translation>Zoznamy odvolaných</translation>
    </message>
    <message>
      <source>Using or exporting private keys will not be possible without providing the correct password</source>
      <translation>Použitie alebo exportovanie súkromných kľúčov nebude možné bez zadania správneho hesla</translation>
    </message>
    <message>
      <source>Database</source>
      <translation>Databáza</translation>
    </message>
    <message>
      <source>The currently used default hash '%1' is insecure. Please select at least 'SHA 224' for security reasons.</source>
      <translation>Aktuálne použitý predvolený odtlačok „%1” nie je bezpečný. Prosím, kvôli bezpečnosti, vyberte aspoň „SHA 224”.</translation>
    </message>
    <message>
      <source>No deleted items found</source>
      <translation>Nenájdené žiadne zmazané položky</translation>
    </message>
    <message>
      <source>Errors detected and repaired while deleting outdated items from the database. A backup file was created</source>
      <translation>Počas odstraňovania starých položiek z databázy boli zistené a opravené chyby. Bol vytvorený záložný súbor</translation>
    </message>
    <message>
      <source>Removing deleted or outdated items from the database failed.</source>
      <translation>Odstránenie starých položiek z databázy zlyhalo.</translation>
    </message>
    <message>
      <source>Recent DataBases</source>
      <translation>Nedávne databázy</translation>
    </message>
    <message>
      <source>System</source>
      <translation>systémový</translation>
    </message>
    <message>
      <source>Croatian</source>
      <translation>chorvátsky</translation>
    </message>
    <message>
      <source>English</source>
      <translation>anglicky</translation>
    </message>
    <message>
      <source>French</source>
      <translation>francúzsky</translation>
    </message>
    <message>
      <source>German</source>
      <translation>nemecky</translation>
    </message>
    <message>
      <source>Russian</source>
      <translation>rusky</translation>
    </message>
    <message>
      <source>Slovak</source>
      <translation>slovensky</translation>
    </message>
    <message>
      <source>Spanish</source>
      <translation>španielsky</translation>
    </message>
    <message>
      <source>Turkish</source>
      <translation>turecky</translation>
    </message>
    <message>
      <source>Language</source>
      <translation>Jazyk</translation>
    </message>
    <message>
      <source>&amp;File</source>
      <translation>&amp;Súbor</translation>
    </message>
    <message>
      <source>&amp;New DataBase</source>
      <translation>&amp;Nová databáza</translation>
    </message>
    <message>
      <source>&amp;Open DataBase</source>
      <translation>&amp;Otvoriť databázu</translation>
    </message>
    <message>
      <source>Set as default DataBase</source>
      <translation>Predvolená databáza</translation>
    </message>
    <message>
      <source>&amp;Close DataBase</source>
      <translation>&amp;Zatvoriť databázu</translation>
    </message>
    <message>
      <source>Options</source>
      <translation>Voľby</translation>
    </message>
    <message>
      <source>Exit</source>
      <translation>Skončiť</translation>
    </message>
    <message>
      <source>I&amp;mport</source>
      <translation>&amp;Importovať</translation>
    </message>
    <message>
      <source>Keys</source>
      <translation>Kľúče</translation>
    </message>
    <message>
      <source>Requests</source>
      <translation>Žiadosti</translation>
    </message>
    <message>
      <source>PKCS#12</source>
      <translation>PKCS#12</translation>
    </message>
    <message>
      <source>PKCS#7</source>
      <translation>PKCS#7</translation>
    </message>
    <message>
      <source>Template</source>
      <translation>Šablóna</translation>
    </message>
    <message>
      <source>Revocation list</source>
      <translation>Zoznam odvolaných</translation>
    </message>
    <message>
      <source>PEM file</source>
      <translation>S&amp;Import starého úbor PEM</translation>
    </message>
    <message>
      <source>Paste PEM file</source>
      <translation>Vložiť súbor PEM</translation>
    </message>
    <message>
      <source>Database dump ( *.dump );; All files ( * )</source>
      <translation>Výpis databázy (*.dump);;Všetky súbory (*)</translation>
    </message>
    <message>
      <source>&amp;Token</source>
      <translation>&amp;Token</translation>
    </message>
    <message>
      <source>&amp;Manage Security token</source>
      <translation>&amp;Spravovať bezpečnostný token</translation>
    </message>
    <message>
      <source>&amp;Init Security token</source>
      <translation>&amp;Inicializovať Bezpečnostný token</translation>
    </message>
    <message>
      <source>&amp;Change PIN</source>
      <translation>Zmeniť @PIN</translation>
    </message>
    <message>
      <source>Change &amp;SO PIN</source>
      <translation>Zmeniť &amp;SO PIN</translation>
    </message>
    <message>
      <source>Init PIN</source>
      <translation>Inicializovať PIN</translation>
    </message>
    <message>
      <source>Extra</source>
      <translation>Extra</translation>
    </message>
    <message>
      <source>&amp;Dump DataBase</source>
      <translation>Vypísať &amp;databázu</translation>
    </message>
    <message>
      <source>&amp;Export Certificate Index</source>
      <translation>&amp;Exportovať index certifikátov</translation>
    </message>
    <message>
      <source>&amp;Export Certificate Index hierarchy</source>
      <translation>&amp;Exportovať hierarchiu indexu certifikátov</translation>
    </message>
    <message>
      <source>C&amp;hange DataBase password</source>
      <translation>Z&amp;meniť heslo databázy</translation>
    </message>
    <message>
      <source>&amp;Import old db_dump</source>
      <translation>&amp;Import starého db_dump</translation>
    </message>
    <message>
      <source>&amp;Undelete items</source>
      <translation>O&amp;bnoviť zmazané</translation>
    </message>
    <message>
      <source>Generate DH parameter</source>
      <translation>Generovať parameter DH</translation>
    </message>
    <message>
      <source>OID Resolver</source>
      <translation>Prekladač OID</translation>
    </message>
    <message>
      <source>&amp;Help</source>
      <translation>&amp;Pomocník</translation>
    </message>
    <message>
      <source>&amp;Content</source>
      <translation>&amp;Obsah</translation>
    </message>
    <message>
      <source>About</source>
      <translation>O programe</translation>
    </message>
    <message>
      <source>Import password</source>
      <translation>Heslo importu</translation>
    </message>
    <message>
      <source>Please enter the password of the old database</source>
      <translation>Prosím, zdajte heslo starej databázy</translation>
    </message>
    <message>
      <source>Password verification error. Ignore keys ?</source>
      <translation>Chyba overenia hesla. Ignorovať kľúče?</translation>
    </message>
    <message>
      <source>Import anyway</source>
      <translation>Importovať i tak</translation>
    </message>
    <message>
      <source>no such option: %1</source>
      <translation>Neznáma voľba: %1</translation>
    </message>
    <message>
      <source>Import PEM data</source>
      <translation>Importovať dáta PEM</translation>
    </message>
    <message>
      <source>Please enter the original SO PIN (PUK) of the token '%1'</source>
      <translation>Prosím, zadajte pôvodný SO PIN (PUK) tokenu „%1”</translation>
    </message>
    <message>
      <source>Search</source>
      <translation>Hľadať</translation>
    </message>
    <message>
      <source>Please enter the new SO PIN (PUK) for the token '%1'</source>
      <translation>Prosím, zadajte nový SO PIN (PUK) tokenu: „%1”</translation>
    </message>
    <message>
      <source>The new label of the token '%1'</source>
      <translation>Nová menovka tokenu „%1”</translation>
    </message>
    <message>
      <source>The token '%1' did not contain any keys or certificates</source>
      <translation>Token „%1” neobsahuje žiadne kľúče ani certifikáty</translation>
    </message>
    <message>
      <source>Current Password</source>
      <translation>Aktuálne heslo</translation>
    </message>
    <message>
      <source>Please enter the current database password</source>
      <translation>Prosím, zadajte heslo aktuálnej databázy</translation>
    </message>
    <message>
      <source>The entered password is wrong</source>
      <translation>Zadané heslo je zlé</translation>
    </message>
    <message>
      <source>New Password</source>
      <translation>Nové heslo</translation>
    </message>
    <message>
      <source>Please enter the new password to encrypt your private keys in the database-file</source>
      <translation>Prosím, zadajte nové heslo na šifrovanie svojich súkromných kľúčov v súbore databázy</translation>
    </message>
    <message>
      <source>Please enter a password, that will be used to encrypt your private keys in the database file:
%1</source>
      <translation>Prosím, zadajte heslo, ktoré bude slúžiť na šifrovanie súkromných kľúčov v súbore databázy:
%1</translation>
    </message>
    <message>
      <source>Password verify error, please try again</source>
      <translation>Chyba overenia hesla, prosím, skúste znova</translation>
    </message>
    <message>
      <source>Password</source>
      <translation>Heslo</translation>
    </message>
    <message>
      <source>Please enter the password for unlocking the database:
%1</source>
      <translation>Prosím, zadajte heslo na odomknutie databázy:
%1</translation>
    </message>
    <message>
      <source>The following error occurred:</source>
      <translation>Nastala nasledujúca chyba:</translation>
    </message>
    <message>
      <source>Copy to Clipboard</source>
      <translation>Kopírovať do schránky</translation>
    </message>
    <message>
      <source>Certificate Index ( index.txt )</source>
      <translation>Index certifikátov (index.txt)</translation>
    </message>
    <message>
      <source>All files ( * )</source>
      <translation>Všetky súbory (*)</translation>
    </message>
    <message>
      <source>Diffie-Hellman parameters are needed for different applications, but not handled by XCA.
Please enter the DH parameter bits</source>
      <translation>Parametre Diffie-Hellman sú potrebné pre rôzne aplikácie, ale nie sú používané v XCA.
Prosím, zadajte bity parametra DH</translation>
    </message>
    <message>
      <source>Error opening file: '%1': %2</source>
      <translation>Chyba otvorenia súboru: „%1”: %2</translation>
    </message>
  </context>
  <context>
    <name>NewCrl</name>
    <message>
      <source>Create CRL</source>
      <translation>Vytvoriť CRL</translation>
    </message>
    <message>
      <source>Last update</source>
      <translation>Posledná aktualizácia</translation>
    </message>
    <message>
      <source>Next update</source>
      <translation>Nasled. aktualizácia</translation>
    </message>
    <message>
      <source>Days</source>
      <translation>Dni</translation>
    </message>
    <message>
      <source>Months</source>
      <translation>Mesiace</translation>
    </message>
    <message>
      <source>Years</source>
      <translation>Roky</translation>
    </message>
    <message>
      <source>Midnight</source>
      <translation>Polnoc</translation>
    </message>
    <message>
      <source>Local time</source>
      <translation>Lokálny čas</translation>
    </message>
    <message>
      <source>Apply</source>
      <translation>Použiť</translation>
    </message>
    <message>
      <source>Options</source>
      <translation>Voľby</translation>
    </message>
    <message>
      <source>CRL number</source>
      <translation>Číslo CRL</translation>
    </message>
    <message>
      <source>Subject alternative name</source>
      <translation>Alternatívne meno predmetu</translation>
    </message>
    <message>
      <source>Revocation reasons</source>
      <translation>Dôvody odvolania</translation>
    </message>
    <message>
      <source>Authority key identifier</source>
      <translation>Identifikátor kľúča autority</translation>
    </message>
    <message>
      <source>Hash algorithm</source>
      <translation>Algoritmus odtlačku</translation>
    </message>
  </context>
  <context>
    <name>NewKey</name>
    <message>
      <source>Please give a name to the new key and select the desired keysize</source>
      <translation>Prosím, zadajte meno nového kľúča a zvoľte jeho požadovanú veľkosť</translation>
    </message>
    <message>
      <source>Key properties</source>
      <translation>Vlastnosti kľúča</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>Názov</translation>
    </message>
    <message>
      <source>The internal name of the new key</source>
      <translation>Interný názov nového kľúča</translation>
    </message>
    <message>
      <source>Curve name</source>
      <translation>Meno krivky</translation>
    </message>
    <message>
      <source>Usually at least 2048 bit keys are recommended</source>
      <translation>Zvyčajne sú odporúčané kľúče aspoň 2048 bitov</translation>
    </message>
    <message>
      <source>New Key</source>
      <translation>Nový kľúč</translation>
    </message>
    <message>
      <source>Keysize</source>
      <translation>Veľkosť kľúča</translation>
    </message>
    <message>
      <source>Keytype</source>
      <translation>Typ kľúča</translation>
    </message>
    <message>
      <source>Remember as default</source>
      <translation>Zapamätať ako predvolené</translation>
    </message>
    <message>
      <source>Create</source>
      <translation>Vytvoriť</translation>
    </message>
  </context>
  <context>
    <name>NewX509</name>
    <message>
      <source>Source</source>
      <translation>Zdroj</translation>
    </message>
    <message>
      <source>Signing request</source>
      <translation>Žiadosť o podpísanie</translation>
    </message>
    <message>
      <source>Show request</source>
      <translation>Zobraziť žiadosť</translation>
    </message>
    <message>
      <source>Sign this Certificate signing &amp;request</source>
      <translation>Podpísať túto Žiadosť o certifikát</translation>
    </message>
    <message>
      <source>Copy extensions from the request</source>
      <translation>Kopírovať rozšírenia zo žiadosti</translation>
    </message>
    <message>
      <source>Modify subject of the request</source>
      <translation>Zmeniť predmet žiadosti</translation>
    </message>
    <message>
      <source>Signing</source>
      <translation>Podpísanie</translation>
    </message>
    <message>
      <source>Create a &amp;self signed certificate with the serial</source>
      <translation>Vytvoriť sebou podpísaný certifikát so sériovým číslom</translation>
    </message>
    <message>
      <source>If you leave this blank the serial 00 will be used</source>
      <translation>Ak toto ponecháte prázdne, bude použité sériové číslo 00</translation>
    </message>
    <message>
      <source>1</source>
      <translation>1</translation>
    </message>
    <message>
      <source>Use &amp;this Certificate for signing</source>
      <translation>Podpísať &amp;týmto certifikátom</translation>
    </message>
    <message>
      <source>All certificates in your database that can create valid signatures</source>
      <translation>Všetky certifikáty v databáze, ktoré môžu vytvoriť platný podpis</translation>
    </message>
    <message>
      <source>Signature algorithm</source>
      <translation>Algoritmus podpisu</translation>
    </message>
    <message>
      <source>Template for the new certificate</source>
      <translation>Šablóna nového certifikátu</translation>
    </message>
    <message>
      <source>All available templates</source>
      <translation>Všetky dostupné šablóny</translation>
    </message>
    <message>
      <source>Apply extensions</source>
      <translation>Pridať rozšírenia</translation>
    </message>
    <message>
      <source>Apply subject</source>
      <translation>Pridať predmet</translation>
    </message>
    <message>
      <source>Apply all</source>
      <translation>Pridať všetko</translation>
    </message>
    <message>
      <source>Subject</source>
      <translation>Predmet</translation>
    </message>
    <message>
      <source>Distinguished name</source>
      <translation>Rozlišovací názov</translation>
    </message>
    <message>
      <source>Add</source>
      <translation>Pridať</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Odstrániť</translation>
    </message>
    <message>
      <source>Private key</source>
      <translation>Súkromný kľúč</translation>
    </message>
    <message>
      <source>This list only contains unused keys</source>
      <translation>Tento zoznam obsahuje len nepoužité kľúče</translation>
    </message>
    <message>
      <source>Used keys too</source>
      <translation>Aj použité kľúče</translation>
    </message>
    <message>
      <source>&amp;Generate a new key</source>
      <translation>&amp;Generovať nový kľúč</translation>
    </message>
    <message>
      <source>Extensions</source>
      <translation>Rozšírenia</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
    <message>
      <source>If this will become a CA certificate or not</source>
      <translation>Či to bude certifikát CA alebo nie</translation>
    </message>
    <message>
      <source>Not defined</source>
      <translation>Nedefinované</translation>
    </message>
    <message>
      <source>Certification Authority</source>
      <translation>Certifikačná autorita</translation>
    </message>
    <message>
      <source>End Entity</source>
      <translation>Koncový certifikát</translation>
    </message>
    <message>
      <source>Path length</source>
      <translation>Dĺžka cesty</translation>
    </message>
    <message>
      <source>How much CAs may be below this.</source>
      <translation>Koľko CA môže byť pod týmto.</translation>
    </message>
    <message>
      <source>The basic constraints should always be critical</source>
      <translation>Základné obmedzenia by mali byť vždy kritické</translation>
    </message>
    <message>
      <source>Key identifier</source>
      <translation>Identifikátor kľúča</translation>
    </message>
    <message>
      <source>Creates a hash of the key following the PKIX guidelines</source>
      <translation>Vytvorí odtlačok kľúča v súlade s odporúčaním PKIX</translation>
    </message>
    <message>
      <source>Copy the Subject Key Identifier from the issuer</source>
      <translation>Kopírovať Identifikátor kľúča predmetu vydavateľa</translation>
    </message>
    <message>
      <source>Validity</source>
      <translation>Platnosť</translation>
    </message>
    <message>
      <source>Not before</source>
      <translation>Nie pred</translation>
    </message>
    <message>
      <source>Not after</source>
      <translation>Nie po</translation>
    </message>
    <message>
      <source>Time range</source>
      <translation>Časový rozsah</translation>
    </message>
    <message>
      <source>Days</source>
      <translation>Dni</translation>
    </message>
    <message>
      <source>Months</source>
      <translation>Mesiace</translation>
    </message>
    <message>
      <source>Years</source>
      <translation>Roky</translation>
    </message>
    <message>
      <source>Apply</source>
      <translation>Použiť</translation>
    </message>
    <message>
      <source>Set the time to 00:00:00 and 23:59:59 respectively</source>
      <translation>Nastavuje čas na 00:00:00 a 23:59:59</translation>
    </message>
    <message>
      <source>Midnight</source>
      <translation>Polnoc</translation>
    </message>
    <message>
      <source>Local time</source>
      <translation>Lokálny čas</translation>
    </message>
    <message>
      <source>No well-defined expiration</source>
      <translation>Bez vypršania</translation>
    </message>
    <message>
      <source>DNS: IP: URI: email: RID:</source>
      <translation>DNS:, IP:, URI:, email:, RID:</translation>
    </message>
    <message>
      <source>Edit</source>
      <translation>Upraviť</translation>
    </message>
    <message>
      <source>URI:</source>
      <translation>URI:</translation>
    </message>
    <message>
      <source>can be altered by the file "aia.txt"</source>
      <translation>možno zmeniť súborom „aia.txt”</translation>
    </message>
    <message>
      <source>Key usage</source>
      <translation>Použitie kľúča</translation>
    </message>
    <message>
      <source>Netscape</source>
      <translation>Netscape</translation>
    </message>
    <message>
      <source>Advanced</source>
      <translation>Pokročilé</translation>
    </message>
    <message>
      <source>Validate</source>
      <translation>Overiť</translation>
    </message>
    <message>
      <source>Create a &amp;self signed certificate with a MD5-hashed QA serial</source>
      <translation>Vytvorí &amp;sebou podpísaný certifikát s sér. číslom QA z odtlačku MD5</translation>
    </message>
    <message>
      <source>This name is only used internally and does not appear in the resulting certificate</source>
      <translation>Tento názov je použitý len interne a neobjaví sa vo výslednom certifikáte</translation>
    </message>
    <message>
      <source>Internal name</source>
      <translation>Interný názov</translation>
    </message>
    <message>
      <source>Critical</source>
      <translation>Kritické</translation>
    </message>
    <message>
      <source>Create Certificate signing request</source>
      <translation>Vytvoriť Žiadosť o podpísanie certifikátu</translation>
    </message>
    <message>
      <source>minimum size: %1</source>
      <translation>minimálna veľkosť: %1</translation>
    </message>
    <message>
      <source>maximum size: %1</source>
      <translation>maximálna veľkosť: %1</translation>
    </message>
    <message>
      <source>only a-z A-Z 0-9 '()+,-./:=?</source>
      <translation>len a-z A-Z 0-9 '()+,-./:=?</translation>
    </message>
    <message>
      <source>only 7-bit clean characters</source>
      <translation>len prosté 7-b znaky</translation>
    </message>
    <message>
      <source>Create XCA template</source>
      <translation>Vytvoriť šablónu XCA</translation>
    </message>
    <message>
      <source>Edit XCA template</source>
      <translation>Upraviť šablónu XCA</translation>
    </message>
    <message>
      <source>Create x509 Certificate</source>
      <translation>Vytvoriť certifikát x509</translation>
    </message>
    <message>
      <source>Other Tabs</source>
      <translation>Záložka Iné</translation>
    </message>
    <message>
      <source>Advanced Tab</source>
      <translation>Záložka Pokročilé</translation>
    </message>
    <message>
      <source>Errors</source>
      <translation>Chyby</translation>
    </message>
    <message>
      <source>From PKCS#10 request</source>
      <translation>Zo žiadosti PKCS#10</translation>
    </message>
    <message>
      <source>Abort rollout</source>
      <translation>Prerušiť operáciu</translation>
    </message>
    <message>
      <source>The following length restrictions of RFC3280 are violated:</source>
      <translation>Nie sú splnené Nasledujúce obmedzenia veľkosti z RFC3280:</translation>
    </message>
    <message>
      <source>Edit subject</source>
      <translation>Upraviť predmet</translation>
    </message>
    <message>
      <source>Continue rollout</source>
      <translation>Pokračovať v operácii</translation>
    </message>
    <message>
      <source>The verification of the Certificate request failed.
The rollout should be aborted.</source>
      <translation>Overenie žiadosti o certifikát zlyhalo.
Operácia by mala byť prerušená.</translation>
    </message>
    <message>
      <source>Continue anyway</source>
      <translation>Pokračovať i tak</translation>
    </message>
    <message>
      <source>The internal name and the common name are empty.
Please set at least the internal name.</source>
      <translation>Interný názov a bežný názov sú prázdne.
Prosím, zadajte aspoň interný názov.</translation>
    </message>
    <message>
      <source>Edit name</source>
      <translation>Upraviť názov</translation>
    </message>
    <message>
      <source>There is no Key selected for signing.</source>
      <translation>Nebol zvolený kľúč na podpísanie.</translation>
    </message>
    <message>
      <source>Select key</source>
      <translation>Vybrať kľúč</translation>
    </message>
    <message>
      <source>The following distinguished name entries are empty:
%1
though you have declared them as mandatory in the options menu.</source>
      <translation>nasledujúce položky rozlišovacieho názvu sú prázdne:
%1
hoci ste ich v nastavení definovali ako povinné.</translation>
    </message>
    <message>
      <source>The key you selected for signing is not a private one.</source>
      <translation>Kľúč, zvolený na podpísanie, nie je súkromný.</translation>
    </message>
    <message>
      <source>Select other signer</source>
      <translation>Zvoliť iného vydavateľa</translation>
    </message>
    <message>
      <source>Select other key</source>
      <translation>Zvoliť iný kľúč</translation>
    </message>
    <message>
      <source>The certificate will be earlier valid than the signer. This is probably not what you want.</source>
      <translation>Certifikát bude platný skôr ako vydavateľ. To asi nie je to, čo chcete.</translation>
    </message>
    <message>
      <source>Edit dates</source>
      <translation>Upraviť dátumy</translation>
    </message>
    <message>
      <source>Adjust date and continue</source>
      <translation>Opraviť dátum a pokračovať</translation>
    </message>
    <message>
      <source>The certificate will be longer valid than the signer. This is probably not what you want.</source>
      <translation>Certifikát bude platný dlhšie ako vydavateľ. To asi nie je to, čo chcete.</translation>
    </message>
    <message>
      <source>The certificate will be out of date before it becomes valid. You most probably mixed up both dates.</source>
      <translation>Certifikát vyprší ešte pred nadobudnutím platnosti. Pravdepodobne ste zamenili oba dátumy.</translation>
    </message>
    <message>
      <source>The certificate contains invalid or duplicate extensions. Check the validation on the advanced tab.</source>
      <translation>Certifikát obsahuje neplatné alebo duplicitné rozšírenia. Skontrolujte platnosť na záložke Pokročilé.</translation>
    </message>
    <message>
      <source>Edit extensions</source>
      <translation>Upraviť rozšírenia</translation>
    </message>
    <message>
      <source>Configfile error on line %1
</source>
      <translation>Chyba konfiguračného súboru v riadku %1
</translation>
    </message>
  </context>
  <context>
    <name>OidResolver</name>
    <message>
      <source>OID Resolver</source>
      <translation>Prekladač OID</translation>
    </message>
    <message>
      <source>Enter the OID, the Nid, or one of the textual representations</source>
      <translation>Zadajte OID, Nid alebo jednu z textových reprezentácií</translation>
    </message>
    <message>
      <source>Search</source>
      <translation>Hľadať</translation>
    </message>
    <message>
      <source>OID</source>
      <translation>OID</translation>
    </message>
    <message>
      <source>Long name</source>
      <translation>Dlhý názov</translation>
    </message>
    <message>
      <source>OpenSSL internal ID</source>
      <translation>Interné ID OpenSSL</translation>
    </message>
    <message>
      <source>Nid</source>
      <translation>Nid</translation>
    </message>
    <message>
      <source>Short name</source>
      <translation>Krátky názov</translation>
    </message>
  </context>
  <context>
    <name>Options</name>
    <message>
      <source>XCA Options</source>
      <translation>Nastavenia XCA</translation>
    </message>
    <message>
      <source>Settings</source>
      <translation>Nastavenia</translation>
    </message>
    <message>
      <source>Default hash algorithm</source>
      <translation>Predvolený algoritmus odtlačku</translation>
    </message>
    <message>
      <source>String types</source>
      <translation>Typy reťazcov</translation>
    </message>
    <message>
      <source>Suppress success messages</source>
      <translation>Potlačiť správy o úspechu</translation>
    </message>
    <message>
      <source>Don't colorize expired certificates</source>
      <translation>Nezvýrazňovať vypršané certifikáty</translation>
    </message>
    <message>
      <source>Translate established x509 terms (%1 -&gt; %2)</source>
      <translation>Preložiť platné položky x509 (%1 -&gt; %2)</translation>
    </message>
    <message>
      <source>The hashing functionality of the token is not used by XCA.
It may however honor a restricted hash-set propagated by the token.
Especially EC and DSA are only defined with SHA1 in the PKCS#11 specification.</source>
      <translation>Hašovacia funkčnosť tokenu nie je v XCA použitá.
Môže však rešpektovať obmedzenú sadu hašovacích algoritmov zverejnených tokenom.
Najmä EC a DSA sú v špecifikácii PKCS#11 definované len s SHA1.</translation>
    </message>
    <message>
      <source>Only use hashes supported by the token when signing with a token key</source>
      <translation>Použiť len odtlačky podporované tokenom pri podpisovaní kľúčom z tokenu</translation>
    </message>
    <message>
      <source>Disable legacy Netscape extensions</source>
      <translation>Vypnúť staré rozšírenia Netscape</translation>
    </message>
    <message>
      <source>Distinguished name</source>
      <translation>Rozlišovací názov</translation>
    </message>
    <message>
      <source>Mandatory subject entries</source>
      <translation>Povinné položky predmetu</translation>
    </message>
    <message>
      <source>Add</source>
      <translation>Pridať</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Odstrániť</translation>
    </message>
    <message>
      <source>Explicit subject entries</source>
      <translation>Explicitné položky predmetu</translation>
    </message>
    <message>
      <source>Default</source>
      <translation>Predvolené</translation>
    </message>
    <message>
      <source>PKCS#11 provider</source>
      <translation>Poskytovateľ PKCS#11</translation>
    </message>
    <message>
      <source>Remove</source>
      <translation>Odstrániť</translation>
    </message>
    <message>
      <source>Search</source>
      <translation>Hľadať</translation>
    </message>
    <message>
      <source>Printable string or UTF8 (default)</source>
      <translation>Tlačiteľné znaky alebo UTF-8 (predvolené)</translation>
    </message>
    <message>
      <source>PKIX recommendation in RFC2459</source>
      <translation>Odporúčania PKIX v RFC2459</translation>
    </message>
    <message>
      <source>No BMP strings, only printable and T61</source>
      <translation>Nie reťazce BMP, len tlačiteľné a T61</translation>
    </message>
    <message>
      <source>UTF8 strings only (RFC2459)</source>
      <translation>Len reťazce UTF-8 (RFC2459)</translation>
    </message>
    <message>
      <source>All strings</source>
      <translation>Všetky reťazce</translation>
    </message>
    <message>
      <source>Load failed</source>
      <translation>Načítanie zlyhalo</translation>
    </message>
  </context>
  <context>
    <name>PwDialog</name>
    <message>
      <source>The password is parsed as 2-digit hex code. It must have an even number of digits (0-9 and a-f)</source>
      <translation>Heslo je spracované ako 2-ciferný šestnástkový kód. Musí mať párny počet číslic (0-9 a a-f)</translation>
    </message>
    <message>
      <source>Take as HEX string</source>
      <translation>Zadajte šestnástkový reťazec</translation>
    </message>
    <message>
      <source>Repeat %1</source>
      <translation>Opakovať %1</translation>
    </message>
    <message>
      <source>%1 mismatch</source>
      <translation>%1 nezhoda</translation>
    </message>
    <message>
      <source>Hex password must only contain the characters '0' - '9' and 'a' - 'f' and it must consist of an even number of characters</source>
      <translation>Šestnástkové heslo môže obsahovať len znaky „0” – „9” a „a” – „f” a musí mať párny počet znakov</translation>
    </message>
    <message>
      <source>E&amp;xit</source>
      <translation>S&amp;končiť</translation>
    </message>
  </context>
  <context>
    <name>QObject</name>
    <message>
      <source>Undefined</source>
      <translation>Nedefinované</translation>
    </message>
    <message>
      <source>Broken / Invalid</source>
      <translation>Poškodený/Neplatný</translation>
    </message>
    <message>
      <source>DB: Rename: '%1' already in use</source>
      <translation>Premenovanie DB: „%1” už je použité</translation>
    </message>
    <message>
      <source>DB: Entry to rename not found: %1</source>
      <translation>DB: Položka na premenovanie nenájdená: %1</translation>
    </message>
    <message>
      <source>DB: Write error %1 - %2</source>
      <translation>DB: Chyba zápisu %1 – %2</translation>
    </message>
    <message>
      <source>Out of data</source>
      <translation>Nedostatok dát</translation>
    </message>
    <message>
      <source>Error finding endmarker of string</source>
      <translation>Chyba nájdenia koncovej značky reťazca</translation>
    </message>
    <message>
      <source>Out of Memory at %1:%2</source>
      <translation>Nedostatok pamäti na %1:%2</translation>
    </message>
    <message>
      <source>Country code</source>
      <translation>Kód krajiny</translation>
    </message>
    <message>
      <source>State or Province</source>
      <translation>Štát alebo provincia</translation>
    </message>
    <message>
      <source>Locality</source>
      <translation>Lokalita</translation>
    </message>
    <message>
      <source>Organisation</source>
      <translation>Organizácia</translation>
    </message>
    <message>
      <source>Organisational unit</source>
      <translation>Organizačná jednotka</translation>
    </message>
    <message>
      <source>Common name</source>
      <translation>Bežný názov</translation>
    </message>
    <message>
      <source>E-Mail address</source>
      <translation>Emailová adresa</translation>
    </message>
    <message>
      <source>Serial number</source>
      <translation>Sériové číslo</translation>
    </message>
    <message>
      <source>Given name</source>
      <translation>Rodné meno</translation>
    </message>
    <message>
      <source>Surname</source>
      <translation>Priezvisko</translation>
    </message>
    <message>
      <source>Title</source>
      <translation>Titul</translation>
    </message>
    <message>
      <source>Initials</source>
      <translation>Iniciály</translation>
    </message>
    <message>
      <source>Description</source>
      <translation>Popis</translation>
    </message>
    <message>
      <source>Role</source>
      <translation>Rola</translation>
    </message>
    <message>
      <source>Pseudonym</source>
      <translation>Pseudonym</translation>
    </message>
    <message>
      <source>Generation Qualifier</source>
      <translation>Prívlastok generácie</translation>
    </message>
    <message>
      <source>x500 Unique Identifier</source>
      <translation>Jedinečný identifikátor x500</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>Názov</translation>
    </message>
    <message>
      <source>DN Qualifier</source>
      <translation>Prívlastok DN</translation>
    </message>
    <message>
      <source>Unstructured name</source>
      <translation>Neštruktúrovaný názov</translation>
    </message>
    <message>
      <source>Challenge password</source>
      <translation>Heslo výzvy</translation>
    </message>
    <message>
      <source>Basic Constraints</source>
      <translation>Základné obmedzenia</translation>
    </message>
    <message>
      <source>Subject alternative name</source>
      <translation>Alternatívne meno predmetu</translation>
    </message>
    <message>
      <source>issuer alternative name</source>
      <translation>Alternatívny názov vydavateľa</translation>
    </message>
    <message>
      <source>Subject key identifier</source>
      <translation>Identifikátor kľúča predmetu</translation>
    </message>
    <message>
      <source>Authority key identifier</source>
      <translation>Identifikátor kľúča autority</translation>
    </message>
    <message>
      <source>Key usage</source>
      <translation>Použitie kľúča</translation>
    </message>
    <message>
      <source>Extended key usage</source>
      <translation>Rozšírené použitie kľúča</translation>
    </message>
    <message>
      <source>CRL distribution points</source>
      <translation>Distribučné body CRL</translation>
    </message>
    <message>
      <source>Authority information access</source>
      <translation>Prístup k informácii CA (AIA)</translation>
    </message>
    <message>
      <source>Certificate type</source>
      <translation>Typ certifikátu</translation>
    </message>
    <message>
      <source>Base URL</source>
      <translation>Základná URL</translation>
    </message>
    <message>
      <source>Revocation URL</source>
      <translation>URL odvolaní</translation>
    </message>
    <message>
      <source>CA Revocation URL</source>
      <translation>URL odvolaní CA</translation>
    </message>
    <message>
      <source>Certificate renewal URL</source>
      <translation>URL obnovenia CRL</translation>
    </message>
    <message>
      <source>CA policy URL</source>
      <translation>URL politiky CA</translation>
    </message>
    <message>
      <source>SSL server name</source>
      <translation>Meno servera SSL</translation>
    </message>
    <message>
      <source>Comment</source>
      <translation>Komentár</translation>
    </message>
    <message>
      <source>All files ( * )</source>
      <translation>Všetky súbory (*)</translation>
    </message>
    <message>
      <source>Import RSA key</source>
      <translation>Importovať kľúč RSA</translation>
    </message>
    <message>
      <source>PKI Keys ( *.pem *.der *.key );; PKCS#8 Keys ( *.p8 *.pk8 );; SSH Public Keys ( *.pub );;</source>
      <translation>Kľúče PKI (*.pem *.der *.key);;Kľúče PKCS#8 (*.p8 *.pk8);;Verejné kľúče SSH (*.pub);;</translation>
    </message>
    <message>
      <source>PKCS#10 CSR ( *.pem *.der *.csr );; Netscape Request ( *.spkac *.spc );;</source>
      <translation>Žiadosť PKCS#10 (*.pem *.der *.csr);;Žiadosť Netscape (*.spkac *.spc);;</translation>
    </message>
    <message>
      <source>Import Request</source>
      <translation>Importovať Žiadosť</translation>
    </message>
    <message>
      <source>Certificates ( *.pem *.der *.crt *.cer );;</source>
      <translation>Certifikáty (*.pem *.der *.crt *.cer);;</translation>
    </message>
    <message>
      <source>Import X.509 Certificate</source>
      <translation>Importovať Certifikát X.509</translation>
    </message>
    <message>
      <source>PKCS#7 data ( *.p7s *.p7m *.p7b );;</source>
      <translation>Dáta PKCS#7 (*.p7s *.p7m *.p7b);;</translation>
    </message>
    <message>
      <source>Import PKCS#7 Certificates</source>
      <translation>Importovať Certifikáty PKCS#7</translation>
    </message>
    <message>
      <source>PKCS#12 Certificates ( *.p12 *.pfx );;</source>
      <translation>Certifikáty PKCS#12 (*.p12 *.pfx);;</translation>
    </message>
    <message>
      <source>Import PKCS#12 Private Certificate</source>
      <translation>Importovať Súkromný certifikát PKCS#12</translation>
    </message>
    <message>
      <source>XCA templates ( *.xca );;</source>
      <translation>Šablóny XCA (*.xca);;</translation>
    </message>
    <message>
      <source>Import XCA Templates</source>
      <translation>Importovať Šablóny XCA</translation>
    </message>
    <message>
      <source>Revocation lists ( *.pem *.der *.crl );;</source>
      <translation>Zoznamy odvolaných (*.pem *.der *.crl);;</translation>
    </message>
    <message>
      <source>Import Certificate Revocation List</source>
      <translation>Importovať Zoznam odvolaných certifikátov</translation>
    </message>
    <message>
      <source>XCA Databases ( *.xdb );;</source>
      <translation>Databázy XCA (*.xdb);;</translation>
    </message>
    <message>
      <source>Open XCA Database</source>
      <translation>Otvoriť databázu XCA</translation>
    </message>
    <message>
      <source>PKCS#11 library ( *.dll );;</source>
      <translation>Knižnica PKCS#11 (*.dll);;</translation>
    </message>
    <message>
      <source>PKCS#11 library ( *.dylib *.so );;</source>
      <translation>Knižnica PKCS#11 (*.dylib *.so);;</translation>
    </message>
    <message>
      <source>PKCS#11 library ( *.so );;</source>
      <translation>Knižnica PKCS#11 (*.so);;</translation>
    </message>
    <message>
      <source>Open PKCS#11 shared library</source>
      <translation>Otvoriť zdieľanú knižnicu PKCS#11</translation>
    </message>
    <message>
      <source>PEM files ( *.pem );;</source>
      <translation>Súbory PEM (*.pem);;</translation>
    </message>
    <message>
      <source>Load PEM encoded file</source>
      <translation>Načítať súbor kódovaný PEM</translation>
    </message>
    <message>
      <source>Please enter the PIN on the PinPad</source>
      <translation>Prosím, zadajte PIN na PinPad-e</translation>
    </message>
    <message>
      <source>Please enter the SO PIN (PUK) of the token %1</source>
      <translation>Prosím, zadajte SO PIN (PUK) tokenu %1</translation>
    </message>
    <message>
      <source>Please enter the PIN of the token %1</source>
      <translation>Prosím, zadajte PIN tokenu %1</translation>
    </message>
    <message>
      <source>No Security token found</source>
      <translation>Nebol nájdený bezpečnostný token</translation>
    </message>
    <message>
      <source>Select</source>
      <translation>Zvoliť</translation>
    </message>
    <message>
      <source>Please enter the new SO PIN (PUK) for the token: '%1'</source>
      <translation>Prosím, zadajte nový SO PIN (PUK) tokenu: „%1”</translation>
    </message>
    <message>
      <source>Please enter the new PIN for the token: '%1'</source>
      <translation>Prosím, zadajte nový PIN tokenu: '%1'</translation>
    </message>
    <message>
      <source>Required PIN size: %1 - %2</source>
      <translation>Požadovaná veľkosť PIN: %1 - %2</translation>
    </message>
    <message>
      <source>Failed to open PKCS11 library: %1</source>
      <translation>Zlyhalo otvorenie knižnice PKCS#11: %1</translation>
    </message>
    <message>
      <source>PKCS#11 function '%1' failed: %2</source>
      <translation>Funkcia PKCS#11 „%1” zlyhala: %2</translation>
    </message>
    <message>
      <source>PKCS#11 function '%1' failed: %2
In library %3
%4</source>
      <translation>Funkcia PKCS#11 „%1” zlyhala: %2
V knižnici %3
%4</translation>
    </message>
    <message>
      <source>Invalid</source>
      <translation>Neplatné</translation>
    </message>
    <message>
      <source>%1 is shorter than %2 bytes: '%3'</source>
      <translation>%1 je kratšie ako %2 B: „%3”</translation>
    </message>
    <message>
      <source>%1 is longer than %2 bytes: '%3'</source>
      <translation>%1 je dlhšie ako %2 B: „%3”</translation>
    </message>
    <message>
      <source>String '%1' for '%2' contains invalid characters</source>
      <translation>Reťazec „%1” pre „%2” obsahuje neplatné znaky</translation>
    </message>
  </context>
  <context>
    <name>ReqTreeView</name>
    <message>
      <source>Sign</source>
      <translation>Podpísať</translation>
    </message>
    <message>
      <source>Similar Request</source>
      <translation>Podobné žiadosti</translation>
    </message>
  </context>
  <context>
    <name>RevocationList</name>
    <message>
      <source>Manage revocations</source>
      <translation>Spravovať odvolania</translation>
    </message>
    <message>
      <source>Add</source>
      <translation>Pridať</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Odstrániť</translation>
    </message>
    <message>
      <source>No.</source>
      <translation>Číslo</translation>
    </message>
    <message>
      <source>Serial</source>
      <translation>Sér. číslo</translation>
    </message>
    <message>
      <source>Revocation</source>
      <translation>Odvolanie</translation>
    </message>
    <message>
      <source>Reason</source>
      <translation>Dôvod</translation>
    </message>
    <message>
      <source>Invalidation</source>
      <translation>Zneplatnenie</translation>
    </message>
    <message>
      <source>Generate CRL</source>
      <translation>Generovať CRL</translation>
    </message>
  </context>
  <context>
    <name>Revoke</name>
    <message>
      <source>Certificate revocation</source>
      <translation>Odvolanie certifikátu</translation>
    </message>
    <message>
      <source>Revocation details</source>
      <translation>Podrobnosti odvolania</translation>
    </message>
    <message>
      <source>Revocation reason</source>
      <translation>Dôvod odvolania</translation>
    </message>
    <message>
      <source>Local time</source>
      <translation>Lokálny čas</translation>
    </message>
    <message>
      <source>Invalid since</source>
      <translation>Neplatný od</translation>
    </message>
    <message>
      <source>Serial</source>
      <translation>Sér. číslo</translation>
    </message>
  </context>
  <context>
    <name>SearchPkcs11</name>
    <message>
      <source>Dialog</source>
      <translation>Dialóg</translation>
    </message>
    <message>
      <source>Directory</source>
      <translation>Adresár</translation>
    </message>
    <message>
      <source>...</source>
      <translation>...</translation>
    </message>
    <message>
      <source>Include subdirectories</source>
      <translation>Zahrnúť podadresáre</translation>
    </message>
    <message>
      <source>Search</source>
      <translation>Hľadať</translation>
    </message>
    <message>
      <source>The following files are possible PKCS#11 libraries</source>
      <translation>nasledujúce súbory sú možné knižnice PKCS#11</translation>
    </message>
  </context>
  <context>
    <name>SelectToken</name>
    <message>
      <source>Select Token</source>
      <translation>Vybrať token</translation>
    </message>
    <message>
      <source>Security token</source>
      <translation>Bezpečnostný token</translation>
    </message>
    <message>
      <source>Please select the security token</source>
      <translation>Prosím, vyberte bezpečnostný token</translation>
    </message>
  </context>
  <context>
    <name>TempTreeView</name>
    <message>
      <source>Duplicate</source>
      <translation>Duplikát</translation>
    </message>
    <message>
      <source>Create certificate</source>
      <translation>Vytvoriť certifikát</translation>
    </message>
    <message>
      <source>Create request</source>
      <translation>Vytvoriť žiadosť</translation>
    </message>
    <message>
      <source>copy</source>
      <translation>kopírovať</translation>
    </message>
  </context>
  <context>
    <name>TrustState</name>
    <message>
      <source>Certificate trust</source>
      <translation>Dôvera certifikátu</translation>
    </message>
    <message>
      <source>Trustment</source>
      <translation>Dôveryhodnosť</translation>
    </message>
    <message>
      <source>&amp;Never trust this certificate</source>
      <translation>&amp;Nikdy nedôverovať tomuto certifikátu</translation>
    </message>
    <message>
      <source>Only &amp;trust this certificate, if we trust the signer</source>
      <translation>&amp;Dôverovať tomuto certifikátu, len ak dôverujem vydavateľovi</translation>
    </message>
    <message>
      <source>&amp;Always trust this certificate</source>
      <translation>&amp;Vždy dôverovať tomuto certifikátu</translation>
    </message>
  </context>
  <context>
    <name>Validity</name>
    <message>
      <source>yyyy-MM-dd hh:mm</source>
      <translation>d. MMM yyyy hh:mm</translation>
    </message>
  </context>
  <context>
    <name>X509SuperTreeView</name>
    <message>
      <source>OpenSSL config</source>
      <translation>Konfigurácia OpenSSL</translation>
    </message>
    <message>
      <source>Transform</source>
      <translation>Transformovať</translation>
    </message>
    <message>
      <source>Template</source>
      <translation>Šablóna</translation>
    </message>
    <message>
      <source>Public key</source>
      <translation>Verejný kľúč</translation>
    </message>
  </context>
  <context>
    <name>XcaTreeView</name>
    <message>
      <source>Subject entries</source>
      <translation>Položky predmetu</translation>
    </message>
    <message>
      <source>X509v3 Extensions</source>
      <translation>Rozšírenia X509v3</translation>
    </message>
    <message>
      <source>Netscape extensions</source>
      <translation>Rozšírenia Netscape</translation>
    </message>
    <message>
      <source>Reset</source>
      <translation>Vymazať</translation>
    </message>
    <message>
      <source>Remove Column</source>
      <translation>Odstrániť stĺpec</translation>
    </message>
    <message>
      <source>Details</source>
      <translation>Podrobnosti</translation>
    </message>
    <message>
      <source>Columns</source>
      <translation>Stĺpce</translation>
    </message>
    <message>
      <source>New</source>
      <translation>Nový</translation>
    </message>
    <message>
      <source>Import</source>
      <translation>Importovať</translation>
    </message>
    <message>
      <source>Paste PEM data</source>
      <translation>Vložiť dáta PEM</translation>
    </message>
    <message>
      <source>Rename</source>
      <translation>Premenovať</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Odstrániť</translation>
    </message>
    <message>
      <source>Export</source>
      <translation>Exportovať</translation>
    </message>
    <message>
      <source>Clipboard</source>
      <translation>Schránka</translation>
    </message>
    <message>
      <source>File</source>
      <translation>Súbor</translation>
    </message>
  </context>
  <context>
    <name>db_base</name>
    <message>
      <source>Bad database item
Name: %1
Type: %2
Size: %3
%4</source>
      <translation>Zlá položka databázy
Názov: %1
typ: %2
Veľkosť: %3
%4</translation>
    </message>
    <message>
      <source>Do you want to delete the item from the database? The bad item may be extracted into a separate file.</source>
      <translation>Chcete odstrániť položku z databázy? Zlá položka môže byť vyňatá do samostatného súboru.</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Odstrániť</translation>
    </message>
    <message>
      <source>Delete and extract</source>
      <translation>Odstrániť a vyňať</translation>
    </message>
    <message>
      <source>Continue</source>
      <translation>Pokračovať</translation>
    </message>
    <message>
      <source>Error opening file: '%1': %2</source>
      <translation>Chyba otvorenia súboru: „%1”: %2</translation>
    </message>
    <message>
      <source>Internal name</source>
      <translation>Interný názov</translation>
    </message>
    <message>
      <source>No.</source>
      <translation>Číslo</translation>
    </message>
    <message>
      <source>How to export the %1 selected items</source>
      <translation>Ako exportovať %1 zvolených položiek</translation>
    </message>
    <message>
      <source>All in one PEM file</source>
      <translation>Všetky v jednom súbore PEM</translation>
    </message>
    <message>
      <source>Each item in one file</source>
      <translation>Každá položka v samostatnom súbore</translation>
    </message>
    <message>
      <source>Save %1 items in one file as</source>
      <translation>Uložiť %1 položiek v jednom súbore ako</translation>
    </message>
    <message>
      <source>PEM files ( *.pem );; All files ( * )</source>
      <translation>Súbory PEM (*.pem);; Všetky súbory (*)</translation>
    </message>
  </context>
  <context>
    <name>db_crl</name>
    <message>
      <source>Signer</source>
      <translation>Vydavateľ</translation>
    </message>
    <message>
      <source>Internal name of the signer</source>
      <translation>Interný názov vydavateľa</translation>
    </message>
    <message>
      <source>No. revoked</source>
      <translation>Č. odvolania</translation>
    </message>
    <message>
      <source>Number of revoked certificates</source>
      <translation>Počet odvolaných certifikátov</translation>
    </message>
    <message>
      <source>Last update</source>
      <translation>Posledná aktualizácia</translation>
    </message>
    <message>
      <source>Next update</source>
      <translation>Nasled. aktualizácia</translation>
    </message>
    <message>
      <source>CRL number</source>
      <translation>Číslo CRL</translation>
    </message>
    <message>
      <source>The revocation list already exists in the database as:
'%1'
and so it was not imported</source>
      <translation>Zoznam odvolaných už v databáze existuje ako:
„%1”,
a tak nebol importovaný</translation>
    </message>
    <message>
      <source>Revocation list export</source>
      <translation>Export Zoznamu odvolaných</translation>
    </message>
    <message>
      <source>CRL ( *.pem *.der *.crl )</source>
      <translation>CRL (*.pem *.der *.crl)</translation>
    </message>
    <message>
      <source>There are no CA certificates for CRL generation</source>
      <translation>Neexistujú certifikáty CA na generovanie CRL</translation>
    </message>
    <message>
      <source>Select CA certificate</source>
      <translation>vyberte certifikát CA</translation>
    </message>
  </context>
  <context>
    <name>db_key</name>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
    <message>
      <source>Size</source>
      <translation>Veľkosť</translation>
    </message>
    <message>
      <source>EC Group</source>
      <translation>Skupina EC</translation>
    </message>
    <message>
      <source>Use</source>
      <translation>Použiť</translation>
    </message>
    <message>
      <source>Password</source>
      <translation>Heslo</translation>
    </message>
    <message>
      <source>The key is already in the database as:
'%1'
and is not going to be imported</source>
      <translation>kľúč už je v databáze ako:
„%1”,
a tak nebude importovaný</translation>
    </message>
    <message>
      <source>The database already contains the public part of the imported key as
'%1
and will be completed by the new, private part of the key</source>
      <translation>Databáza už obsahuje verejnú časť importovaného kľúča ako
„%1”
a bude doplnená novou, súkromnou, časťou kľúča</translation>
    </message>
    <message>
      <source>Key size too small !</source>
      <translation>Dĺžka kľúča je príliš malá!</translation>
    </message>
    <message>
      <source>You are sure to create a key of the size: %1 ?</source>
      <translation>naozaj chcete vytvoriť kľúč s veľkosťou: %1?</translation>
    </message>
    <message>
      <source>PEM public</source>
      <translation>PEM verejný</translation>
    </message>
    <message>
      <source>SSH2 public</source>
      <translation>SSH2 verejný</translation>
    </message>
    <message>
      <source>PEM private</source>
      <translation>PEM súkromný</translation>
    </message>
    <message>
      <source>Export keys to Clipboard</source>
      <translation>Exportovať kľúče do schránky</translation>
    </message>
    <message>
      <source>Clipboard</source>
      <translation>Schránka</translation>
    </message>
    <message>
      <source>Export public key [%1]</source>
      <translation>Exportovať verejný kľúč [%1]</translation>
    </message>
    <message>
      <source>DER public</source>
      <translation>DER verejný</translation>
    </message>
    <message>
      <source>DER private</source>
      <translation>DER súkromný</translation>
    </message>
    <message>
      <source>PEM encryped</source>
      <translation>PEM šifrovaný</translation>
    </message>
    <message>
      <source>PKCS#8 encrypted</source>
      <translation>PKCS#8 šifrovaný</translation>
    </message>
    <message>
      <source>Export private key [%1]</source>
      <translation>Exportovať súkromný kľúč [%1]</translation>
    </message>
    <message>
      <source>Private Keys ( *.pem *.der *.pk8 );; SSH Public Keys ( *.pub )</source>
      <translation>Súkromné kľúče (*.pem *.der *.pk8);; Verejné kľúče SSH (*.pub)</translation>
    </message>
    <message>
      <source>Tried to change password of a token</source>
      <translation>Vyskúšaná zmena hesla tokenu</translation>
    </message>
  </context>
  <context>
    <name>db_temp</name>
    <message>
      <source>Bad template: %1</source>
      <translation>Zlá šablóna: %1</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
    <message>
      <source>Nothing</source>
      <translation>Nič</translation>
    </message>
    <message>
      <source>Preset Template values</source>
      <translation>Predvoľby hodnôt zo šablóny</translation>
    </message>
    <message>
      <source>Save template as</source>
      <translation>Uložiť šablónu ako</translation>
    </message>
    <message>
      <source>XCA templates ( *.xca );; All files ( * )</source>
      <translation>Šablóny XCA (*.xca);; Všetky súbory (*)</translation>
    </message>
  </context>
  <context>
    <name>db_x509</name>
    <message>
      <source>CA</source>
      <translation>CA</translation>
    </message>
    <message>
      <source>reflects the basic Constraints extension</source>
      <translation>Odráža rozšírenie Základné obmedzenia</translation>
    </message>
    <message>
      <source>Serial</source>
      <translation>Sér. číslo</translation>
    </message>
    <message>
      <source>Start date</source>
      <translation>Dátum začiatku</translation>
    </message>
    <message>
      <source>Expiry date</source>
      <translation>Dátum vypršania</translation>
    </message>
    <message>
      <source>MD5 fingerprint</source>
      <translation>Odtlačok MD5</translation>
    </message>
    <message>
      <source>SHA1 fingerprint</source>
      <translation>Odtlačok SHA1</translation>
    </message>
    <message>
      <source>SHA256 fingerprint</source>
      <translation>Odtlačok SHA256</translation>
    </message>
    <message>
      <source>Not before</source>
      <translation>Nie pred</translation>
    </message>
    <message>
      <source>Not after</source>
      <translation>Nie po</translation>
    </message>
    <message>
      <source>Trust state</source>
      <translation>Stav dôvery</translation>
    </message>
    <message>
      <source>Revocation</source>
      <translation>Odvolanie</translation>
    </message>
    <message>
      <source>CRL Expiration</source>
      <translation>CRL vypršania</translation>
    </message>
    <message>
      <source>Plain View</source>
      <translation>Prosté zobrazenie</translation>
    </message>
    <message>
      <source>Tree View</source>
      <translation>Stromové zobrazenie</translation>
    </message>
    <message>
      <source>The certificate already exists in the database as:
'%1'
and so it was not imported</source>
      <translation>Certifikát v databáze už existuje ako:
„%1”,
a tak nebol importovaný</translation>
    </message>
    <message>
      <source>Invalid public key</source>
      <translation>Neplatný verejný kľúč</translation>
    </message>
    <message>
      <source>Please enter the new hexadecimal secret number for the QA process.</source>
      <translation>Prosím zadajte šestnástkové tajné číslo procesu QA.</translation>
    </message>
    <message>
      <source>The QA process has been terminated by the user.</source>
      <translation>Proces QA bol prerušený používateľom.</translation>
    </message>
    <message>
      <source>The key you selected for signing is not a private one.</source>
      <translation>Kľúč, zvolený na podpísanie, nie je súkromným.</translation>
    </message>
    <message>
      <source>Store the certificate to the key on the token '%1 (#%2)' ?</source>
      <translation>Uložiť certifikát do kľúča na tokene „%1 (#%2)”?</translation>
    </message>
    <message>
      <source>PEM chain</source>
      <translation>Reťaz PEM</translation>
    </message>
    <message>
      <source>PKCS#7 chain</source>
      <translation>Reťaz PKCS #7</translation>
    </message>
    <message>
      <source>PKCS#12 chain</source>
      <translation>Reťaz PKCS#12</translation>
    </message>
    <message>
      <source>PKCS#7 trusted</source>
      <translation>PKCS#7 dôveryhodný</translation>
    </message>
    <message>
      <source>PKCS#7 all</source>
      <translation>PKCS#7 všetky</translation>
    </message>
    <message>
      <source>PEM + key</source>
      <translation>PEM + kľúč</translation>
    </message>
    <message>
      <source>PEM trusted</source>
      <translation>PEM dôveryhodný</translation>
    </message>
    <message>
      <source>PEM all</source>
      <translation>PEM všetky</translation>
    </message>
    <message>
      <source>Certificate Index file</source>
      <translation>Súbor indexu certifikátov</translation>
    </message>
    <message>
      <source>Certificate export</source>
      <translation>Export certifikátu</translation>
    </message>
    <message>
      <source>X509 Certificates ( *.pem *.cer *.crt *.p12 *.p7b )</source>
      <translation>Certifikáty X509 (*.pem *.cer *.crt *.p12 *.p7b)</translation>
    </message>
    <message>
      <source>There was no key found for the Certificate: '%1'</source>
      <translation>Nebol nájdený kľúč certifikátu: „%1”</translation>
    </message>
    <message>
      <source>Not possible for a token key: '%1'</source>
      <translation>Nie je možné pre kľúč tokenu: „%1”</translation>
    </message>
    <message>
      <source>Not possible for the token-key Certificate '%1'</source>
      <translation>Nie je možné pre Certifikát kľúča tokenu „%1”</translation>
    </message>
    <message>
      <source> days</source>
      <translation> dní</translation>
    </message>
  </context>
  <context>
    <name>db_x509name</name>
    <message>
      <source>Subject</source>
      <translation>Predmet</translation>
    </message>
    <message>
      <source>Complete distinguished name</source>
      <translation>Úplný rozlišovací názov</translation>
    </message>
    <message>
      <source>Subject hash</source>
      <translation>Odtlačok predmetu</translation>
    </message>
    <message>
      <source>Hash to lookup certs in directories</source>
      <translation>Odtlačok na hľadanie certifikátov v adresároch</translation>
    </message>
  </context>
  <context>
    <name>db_x509req</name>
    <message>
      <source>Signed</source>
      <translation>Podpísaný</translation>
    </message>
    <message>
      <source>whether the request is already signed or not</source>
      <translation>Či už bola žiadosť podpísaná alebo nie</translation>
    </message>
    <message>
      <source>Unstructured name</source>
      <translation>Neštruktúrovaný názov</translation>
    </message>
    <message>
      <source>Challenge password</source>
      <translation>Heslo výzvy</translation>
    </message>
    <message>
      <source>The certificate signing request already exists in the database as
'%1'
and thus was not stored</source>
      <translation>Žiadosť o podpísanie certifikátu už v databáze existuje ak
„%1”,
a tak nebola uložená</translation>
    </message>
    <message>
      <source>Certificate request export</source>
      <translation>Export Žiadosti o certifikát</translation>
    </message>
    <message>
      <source>Certificate request ( *.pem *.der *.csr )</source>
      <translation>Žiadosť o certifikát (*.pem *.der *.csr)</translation>
    </message>
  </context>
  <context>
    <name>db_x509super</name>
    <message>
      <source>Key name</source>
      <translation>Názov kľúča</translation>
    </message>
    <message>
      <source>Internal name of the key</source>
      <translation>Interný názov kľúča</translation>
    </message>
    <message>
      <source>Signature algorithm</source>
      <translation>Algoritmus podpisu</translation>
    </message>
    <message>
      <source>Save as OpenSSL config</source>
      <translation>Uložiť ako konfiguráciu OpenSSL</translation>
    </message>
    <message>
      <source>Config files ( *.conf *.cnf);; All files ( * )</source>
      <translation>Konfiguračné súbory (*.conf *.cnf);; Všetky súbory (*)</translation>
    </message>
    <message>
      <source>The following extensions were not ported into the template</source>
      <translation>Do šablóny neboli prenesené nasledujúce rozšírenia</translation>
    </message>
  </context>
  <context>
    <name>kvView</name>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
    <message>
      <source>Content</source>
      <translation>Obsah</translation>
    </message>
  </context>
  <context>
    <name>pass_info</name>
    <message>
      <source>Password</source>
      <translation>Heslo</translation>
    </message>
    <message>
      <source>PIN</source>
      <translation>PIN</translation>
    </message>
  </context>
  <context>
    <name>pki_base</name>
    <message>
      <source>Error opening file: '%1': %2</source>
      <translation>Chyba otvorenia súboru „%1”: %2</translation>
    </message>
    <message>
      <source>Error writing to file: '%1': %2</source>
      <translation>Chyba zápisu do súboru „%1”: %2</translation>
    </message>
    <message>
      <source>Error: </source>
      <translation>Chyba: </translation>
    </message>
    <message>
      <source>Internal error: Unexpected message: %1 %2</source>
      <translation>Interná chyba: Neočakávaná správa: %1 %2</translation>
    </message>
  </context>
  <context>
    <name>pki_crl</name>
    <message>
      <source>Successfully imported the revocation list '%1'</source>
      <translation>Úspešne importovaný Zoznam odvolaní „%1”</translation>
    </message>
    <message>
      <source>Delete the revocation list '%1'?</source>
      <translation>Odstrániť Zoznam odvolaní „%1”?</translation>
    </message>
    <message>
      <source>Successfully created the revocation list '%1'</source>
      <translation>Úspešne vytvorený Zoznam odvolaní „%1”</translation>
    </message>
    <message>
      <source>Delete the %1 revocation lists: %2?</source>
      <translation>Odstrániť %1 Zoznamov odvolaní: %2?</translation>
    </message>
    <message>
      <source>Unable to load the revocation list in file %1. Tried PEM and DER formatted CRL.</source>
      <translation>Nemožno načítať zoznam odvolaní zo súboru %1. Vyskúšaný PEM a DER formát CRL.</translation>
    </message>
    <message>
      <source>No issuer given</source>
      <translation>Nebol zadaný vydavateľ</translation>
    </message>
    <message>
      <source>Wrong Size %1</source>
      <translation>Zlá veľkosť %1</translation>
    </message>
    <message>
      <source>unknown</source>
      <translation>neznáme</translation>
    </message>
  </context>
  <context>
    <name>pki_evp</name>
    <message>
      <source>Failed to decrypt the key (bad password) </source>
      <translation>Zlyhalo dešifrovanie kľúča (zlé heslo) </translation>
    </message>
    <message>
      <source>Please enter the password to decrypt the private key.</source>
      <translation>Prosím, zadajte heslo na dešifrovanie súkromného kľúča.</translation>
    </message>
    <message>
      <source>Please enter the password to decrypt the private key from file:
%1</source>
      <translation>Prosím, zadajte heslo na dešifrovanie súkromného kľúča zo súboru:
%1</translation>
    </message>
    <message>
      <source>Unable to load the private key in file %1. Tried PEM and DER private, public, PKCS#8 key types and SSH2 format.</source>
      <translation>Nemožno načítať súkromný kľúč zo súboru %1. Vyskúšaný PEM a DER súkromný, verejný, PKCS#8 typy kľúčov a formát SSH2.</translation>
    </message>
    <message>
      <source>Ignoring unsupported private key</source>
      <translation>Ignorujem nepodporovaný súkromný kľúč</translation>
    </message>
    <message>
      <source>Please enter the password to decrypt the private key: '%1'</source>
      <translation>Prosím, zadajte heslo na dešifrovanie súkromného kľúča: „%1”</translation>
    </message>
    <message>
      <source>Password input aborted</source>
      <translation>Zadávanie hesla zrušené</translation>
    </message>
    <message>
      <source>Please enter the database password for decrypting the key '%1'</source>
      <translation>Prosím, zadajte heslo databázy na dešifrovanie kľúča „%1”</translation>
    </message>
    <message>
      <source>Please enter the password to protect the private key: '%1'</source>
      <translation>Prosím, zadajte heslo na ochranu súkromného kľúča: „%1”</translation>
    </message>
    <message>
      <source>Please enter the database password for encrypting the key</source>
      <translation>Prosím, zadajte heslo databázy na zašifrovanie kľúča</translation>
    </message>
    <message>
      <source>Please enter the password protecting the PKCS#8 key '%1'</source>
      <translation>Prosím, zadajte heslo na ochranu kľúča PKCS#8 „%1”</translation>
    </message>
    <message>
      <source>Please enter the export password for the private key '%1'</source>
      <translation>Prosím, zadajte heslo exportu súkromného kľúča „%1”</translation>
    </message>
  </context>
  <context>
    <name>pki_key</name>
    <message>
      <source>Successfully imported the %1 public key '%2'</source>
      <translation>Úspešne importovaný %1 verejný kľúč „%2”</translation>
    </message>
    <message>
      <source>Delete the %1 public key '%2'?</source>
      <translation>Odstrániť %1 verejný kľúč „%2”?</translation>
    </message>
    <message>
      <source>Successfully imported the %1 private key '%2'</source>
      <translation>Úspešne importovaný %1 súkromný kľúč „%2”</translation>
    </message>
    <message>
      <source>Delete the %1 private key '%2'?</source>
      <translation>Odstrániť %1 súkromný kľúč „%2”?</translation>
    </message>
    <message>
      <source>Successfully created the %1 private key '%2'</source>
      <translation>Úspešne vytvorený %1 súkromný kľúč „%2”</translation>
    </message>
    <message>
      <source>Delete the %1 keys: %2?</source>
      <translation>Odstrániť %1 kľúčov: %2?</translation>
    </message>
    <message>
      <source>Public key</source>
      <translation>Verejný kľúč</translation>
    </message>
    <message>
      <source>Common</source>
      <translation>Spoločné</translation>
    </message>
    <message>
      <source>Private</source>
      <translation>Súkromné</translation>
    </message>
    <message>
      <source>Bogus</source>
      <translation>Falošné</translation>
    </message>
    <message>
      <source>PIN</source>
      <translation>PIN</translation>
    </message>
    <message>
      <source>No password</source>
      <translation>Bez hesla</translation>
    </message>
    <message>
      <source>Invalid SSH2 public key</source>
      <translation>Neplatný verejný kľúč SSH2</translation>
    </message>
    <message>
      <source>Failed writing to %1</source>
      <translation>Zlyhal zápis do %1</translation>
    </message>
  </context>
  <context>
    <name>pki_multi</name>
    <message>
      <source>No known PEM encoded items found</source>
      <translation>Nájdené neznáme kódované položky PEM</translation>
    </message>
  </context>
  <context>
    <name>pki_pkcs12</name>
    <message>
      <source>Please enter the password to decrypt the PKCS#12 file:
%1</source>
      <translation>Prosím, zadajte heslo na dešifrovanie súboru PKCS#12:
%1</translation>
    </message>
    <message>
      <source>Unable to load the PKCS#12 (pfx) file %1.</source>
      <translation>Nemožno načítať súbor PKCS#12 (pfx) %1.</translation>
    </message>
    <message>
      <source>The supplied password was wrong (%1)</source>
      <translation>Zadané heslo je zlé (%1)</translation>
    </message>
    <message>
      <source>Please enter the password to encrypt the PKCS#12 file</source>
      <translation>Prosím, zadajte heslo na zašifrovanie súboru PKCS#12</translation>
    </message>
    <message>
      <source>No key or no Cert and no pkcs12</source>
      <translation>Nie je to kľúč, ani Certifikát a ani PKCS#12</translation>
    </message>
  </context>
  <context>
    <name>pki_pkcs7</name>
    <message>
      <source>Unable to load the PKCS#7 file %1. Tried PEM and DER format.</source>
      <translation>Nemožno načítať súbor PKCS#7 %1. Vyskúšaný formát PEM a DER.</translation>
    </message>
  </context>
  <context>
    <name>pki_scard</name>
    <message>
      <source>Successfully imported the token key '%1'</source>
      <translation>Úspešne importovaný kľúč tokenu „%1”</translation>
    </message>
    <message>
      <source>Delete the token key '%1'?</source>
      <translation>Odstrániť kľúč tokenu „%1”?</translation>
    </message>
    <message>
      <source>Successfully created the token key '%1'</source>
      <translation>úspešne vytvorený kľúč tokenu „%1”</translation>
    </message>
    <message>
      <source>Delete the %1 keys: %2?</source>
      <translation>Odstrániť %1 kľúčov: „%2”?</translation>
    </message>
    <message>
      <source>Delete the private key '%1' from the token '%2 (#%3)' ?</source>
      <translation>Odstrániť súkromný kľúč „%1” z tokenu „%2 (#%3)”?</translation>
    </message>
    <message>
      <source>This Key is already on the token</source>
      <translation>Tento kľúč už je na tokene</translation>
    </message>
    <message>
      <source>PIN input aborted</source>
      <translation>Zadanie PIN zrušené</translation>
    </message>
    <message>
      <source>Unable to find copied key on the token</source>
      <translation>Nemožno nájsť kópiu kľúča na tokene</translation>
    </message>
    <message>
      <source>Please insert card: %1 %2 [%3] with Serial: %4</source>
      <translation>Prosím, vložte kartu: %1 %2 [%3] so sér. číslom: %4</translation>
    </message>
    <message>
      <source>Public Key mismatch. Please re-import card</source>
      <translation>Nezhoda verejného kľúča. Prosím, znova importujte kartu</translation>
    </message>
    <message>
      <source>Illegal Key generation method</source>
      <translation>Neprípustná metóda generovania kľúča</translation>
    </message>
    <message>
      <source>Unable to find generated key on card</source>
      <translation>Nemožno nájsť vygenerovaný kľúč na karte</translation>
    </message>
    <message>
      <source>Ignoring unsupported token key</source>
      <translation>Ignorovanie nepodporovaného kľúča tokenu</translation>
    </message>
    <message>
      <source>Wrong Size %1</source>
      <translation>Zlá veľkosť %1</translation>
    </message>
    <message>
      <source>Token %1</source>
      <translation>Token %1</translation>
    </message>
    <message>
      <source>Failed to find the key on the token</source>
      <translation>Zlyhalo nájdenie kľúča na tokene</translation>
    </message>
    <message>
      <source>Invalid Pin for the token</source>
      <translation>Neplatný PIN tokenu</translation>
    </message>
    <message>
      <source>Failed to initialize the key on the token</source>
      <translation>Zlyhal inicializácia kľúča na tokene</translation>
    </message>
  </context>
  <context>
    <name>pki_temp</name>
    <message>
      <source>Successfully imported the XCA template '%1'</source>
      <translation>Úspešne importovaná šablóna XCA „%1”</translation>
    </message>
    <message>
      <source>Delete the XCA template '%1'?</source>
      <translation>Odstrániť šablónu XCA „%1”?</translation>
    </message>
    <message>
      <source>Successfully created the XCA template '%1'</source>
      <translation>Úspešne vytvorená šablóna XCA „%1”</translation>
    </message>
    <message>
      <source>Delete the %1 XCA templates: %2?</source>
      <translation>Odstrániť %1 šablón XCA: %2?</translation>
    </message>
    <message>
      <source>Wrong Size %1</source>
      <translation>Zlá veľkosť %1</translation>
    </message>
    <message>
      <source>Template file content error (too small)</source>
      <translation>Chyba obsahu súboru šablóny (príliš malá)</translation>
    </message>
    <message>
      <source>Template file content error (bad size)</source>
      <translation>Chyba obsahu súboru šablóny (zlá veľkosť)</translation>
    </message>
    <message>
      <source>Template file content error (too small): %1</source>
      <translation>Chyba obsahu súboru šablóny (príliš malá): %1</translation>
    </message>
    <message>
      <source>Not a PEM encoded XCA Template</source>
      <translation>Šablóna XCA nie je vo formáte PEM</translation>
    </message>
    <message>
      <source>Not an XCA Template, but '%1'</source>
      <translation>Nie je šablóna XCA, ale „%1”</translation>
    </message>
  </context>
  <context>
    <name>pki_x509</name>
    <message>
      <source>Successfully imported the certificate '%1'</source>
      <translation>Úspešne importovaný certifikát „%1”</translation>
    </message>
    <message>
      <source>Delete the certificate '%1'?</source>
      <translation>Odstrániť certifikát „%1”?</translation>
    </message>
    <message>
      <source>Successfully created the certificate '%1'</source>
      <translation>Úspešne vytvorený certifikát „%1”</translation>
    </message>
    <message>
      <source>Delete the %1 certificates: %2?</source>
      <translation>Odstrániť %1 certifikátov: %2?</translation>
    </message>
    <message>
      <source>Unable to load the certificate in file %1. Tried PEM and DER certificate.</source>
      <translation>Nemožno načítať certifikát zo súboru %1. Vyskúšaný certifikát PEM a DER.</translation>
    </message>
    <message>
      <source>This certificate is already on the security token</source>
      <translation>Tento certifikát už je na bezpečnostnom tokene</translation>
    </message>
    <message>
      <source>Delete the certificate '%1' from the token '%2 (#%3)'?</source>
      <translation>Odstrániť certifikát „%1” z tokenu „%2 (#%3)”?</translation>
    </message>
    <message>
      <source>There is no key for signing !</source>
      <translation>Chýba kľúč na podpísanie!</translation>
    </message>
    <message>
      <source>Wrong Size %1</source>
      <translation>Zlá veľkosť %1</translation>
    </message>
    <message>
      <source>Not trusted</source>
      <translation>Nedôverovať</translation>
    </message>
    <message>
      <source>Trust inherited</source>
      <translation>Dôvera zdedená</translation>
    </message>
    <message>
      <source>Always Trusted</source>
      <translation>Vždy dôverovať</translation>
    </message>
    <message>
      <source>No</source>
      <translation>Nie</translation>
    </message>
    <message>
      <source>Yes</source>
      <translation>Áno</translation>
    </message>
  </context>
  <context>
    <name>pki_x509req</name>
    <message>
      <source>Signing key not valid (public key)</source>
      <translation>Podpisujúci kľúč neplatný (verejný kľúč)</translation>
    </message>
    <message>
      <source>Successfully imported the %1 certificate request '%2'</source>
      <translation>úspešne importovaná %1 žiadosť o certifikát „%2”</translation>
    </message>
    <message>
      <source>Delete the %1 certificate request '%2'?</source>
      <translation>Odstrániť %1 žiadosť o certifikát „%2”?</translation>
    </message>
    <message>
      <source>Successfully created the %1 certificate request '%2'</source>
      <translation>Úspešne vytvorená %1 žiadosť o certifikát „%2”</translation>
    </message>
    <message>
      <source>Delete the %1 certificate requests: %2?</source>
      <translation>Odstrániť %1 žiadosť o certifikát: %2?</translation>
    </message>
    <message>
      <source>Unable to load the certificate request in file %1. Tried PEM, DER and SPKAC format.</source>
      <translation>Nemožno načítať žiadosť o certifikát zo súboru %1. Vyskúšané formáty PEM, DER a SPKAC.</translation>
    </message>
    <message>
      <source>Signed</source>
      <translation>Podpísaný</translation>
    </message>
    <message>
      <source>Unhandled</source>
      <translation>Nespracované</translation>
    </message>
    <message>
      <source>Wrong Size %1</source>
      <translation>Zlá veľkosť %1</translation>
    </message>
  </context>
  <context>
    <name>v3ext</name>
    <message>
      <source>Add</source>
      <translation>Pridať</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Odstrániť</translation>
    </message>
    <message>
      <source>Apply</source>
      <translation>Použiť</translation>
    </message>
    <message>
      <source>Validate</source>
      <translation>Overiť</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Zrušiť</translation>
    </message>
    <message>
      <source>An email address or 'copy'</source>
      <translation>Emailová adresa alebo „copy”</translation>
    </message>
    <message>
      <source>An email address</source>
      <translation>Emailová adresa</translation>
    </message>
    <message>
      <source>a registered ID: OBJECT IDENTIFIER</source>
      <translation>Registrované ID: IDENTIFIKÁTOR OBJEKTU</translation>
    </message>
    <message>
      <source>a uniform resource indicator</source>
      <translation>Jednotný identifikátor zdroja</translation>
    </message>
    <message>
      <source>a DNS domain name</source>
      <translation>Doménové meno DNS</translation>
    </message>
    <message>
      <source>an IP address</source>
      <translation>Adresa IP</translation>
    </message>
    <message>
      <source>Syntax: &lt;OID&gt;;TYPE:text like '1.2.3.4:UTF8:name'</source>
      <translation>Syntax: &lt;OID&gt;;TYP:text ako '1.2.3.4:UTF8:názov'</translation>
    </message>
    <message>
      <source>No editing. Only 'copy' allowed here</source>
      <translation>Neupravovať. Je tu dovolené len „copy”</translation>
    </message>
    <message>
      <source>Validation failed:
'%1'
%2</source>
      <translation>Overenie zlyhalo:
„%1”
%2</translation>
    </message>
    <message>
      <source>Validation successful:
'%1'</source>
      <translation>Overenie úspešné:
„%1”</translation>
    </message>
  </context>
</TS>
