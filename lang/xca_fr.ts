<?xml version="1.0"?>
<TS>
  <context>
    <name>About</name>
    <message>
      <source>Done</source>
      <translation>Terminé</translation>
    </message>
  </context>
  <context>
    <name>CaProperties</name>
    <message>
      <source>CA Properties</source>
      <translation>Propriétés du CA</translation>
    </message>
    <message>
      <source>Use random Serial numbers</source>
      <translation>Utiliser des numéros de série aléatoires</translation>
    </message>
    <message>
      <source>Days until next CRL issuing</source>
      <translation>Nombre de jours avant la génération de la prochaine liste de révocation</translation>
    </message>
    <message>
      <source>Default template</source>
      <translation>Modèle par défaut</translation>
    </message>
    <message>
      <source>Next serial for signing</source>
      <translation>Numéro de série suivant pour la signature</translation>
    </message>
  </context>
  <context>
    <name>CertDetail</name>
    <message>
      <source>Details of the Certificate</source>
      <translation>Détails du Certificat</translation>
    </message>
    <message>
      <source>S&amp;tatus</source>
      <translation>E&amp;tat</translation>
    </message>
    <message>
      <source>Serial</source>
      <translation>Numéro de série</translation>
    </message>
    <message>
      <source>The serial number of the certificate</source>
      <translation>Le numéro de série du certificat</translation>
    </message>
    <message>
      <source>The internal name of the certificate in the database</source>
      <translation>Le nom interne du certificat dans la base de données</translation>
    </message>
    <message>
      <source>Internal name</source>
      <translation>Nom interne</translation>
    </message>
    <message>
      <source>Signature algorithm</source>
      <translation>Algorithme de signature</translation>
    </message>
    <message>
      <source>Signature</source>
      <translation>Signature</translation>
    </message>
    <message>
      <source>Key</source>
      <translation>Clé</translation>
    </message>
    <message>
      <source>Fingerprints</source>
      <translation>Empreinte</translation>
    </message>
    <message>
      <source>MD5</source>
      <translation>MD5</translation>
    </message>
    <message>
      <source>An md5 hashsum of the certificate</source>
      <translation>La somme de hachage MD5 du certificat</translation>
    </message>
    <message>
      <source>SHA1</source>
      <translation>SHA1</translation>
    </message>
    <message>
      <source>A SHA-1 hashsum of the certificate</source>
      <translation>La somme de hachage SHA-1 du certificat</translation>
    </message>
    <message>
      <source>SHA256</source>
      <translation>SHA256</translation>
    </message>
    <message>
      <source>A SHA-256 hashsum of the certificate</source>
      <translation>La somme de hachage SHA-1 du certificat</translation>
    </message>
    <message>
      <source>Validity</source>
      <translation>Validité</translation>
    </message>
    <message>
      <source>The time since the certificate is valid</source>
      <translation>Le moment depuis lequel le certificat est valide</translation>
    </message>
    <message>
      <source>The time until the certificate is valid</source>
      <translation>Le moment auquel le certificat échoit</translation>
    </message>
    <message>
      <source>&amp;Subject</source>
      <translation>&amp;Sujet</translation>
    </message>
    <message>
      <source>&amp;Issuer</source>
      <translation>&amp;Emetteur</translation>
    </message>
    <message>
      <source>Attributes</source>
      <translation>Attributs</translation>
    </message>
    <message>
      <source>&amp;Extensions</source>
      <translation>E&amp;xtensions</translation>
    </message>
    <message>
      <source>Show config</source>
      <translation>Afficher la configuration</translation>
    </message>
    <message>
      <source>Show extensions</source>
      <translation>Afficher les extensions</translation>
    </message>
    <message>
      <source>Not available</source>
      <translation>Non disponible</translation>
    </message>
    <message>
      <source>Signer unknown</source>
      <translation>Signataire inconnu</translation>
    </message>
    <message>
      <source>Self signed</source>
      <translation>Auto-signé</translation>
    </message>
    <message>
      <source>Not trusted</source>
      <translation>Pas sûr</translation>
    </message>
    <message>
      <source>Trusted</source>
      <translation>Sûr</translation>
    </message>
    <message>
      <source>Revoked: </source>
      <translation>Révoqués: </translation>
    </message>
    <message>
      <source>Not valid</source>
      <translation>Echu</translation>
    </message>
    <message>
      <source>Valid</source>
      <translation>Valide</translation>
    </message>
    <message>
      <source>Details of the certificate signing request</source>
      <translation>Détails de la requête de signature</translation>
    </message>
  </context>
  <context>
    <name>CertExtend</name>
    <message>
      <source>Certificate renewal</source>
      <translation>Renouvellement du certificat</translation>
    </message>
    <message>
      <source>This will create a new certificate as a copy of the old one with a new serial number and adjusted validity values.</source>
      <translation>Créer un nouveau certificat conforme à l'ancien, mais avec un nouveau numéro de série et les limites de validité ajustées.</translation>
    </message>
    <message>
      <source>Validity</source>
      <translation>Validité</translation>
    </message>
    <message>
      <source>Not before</source>
      <translation>Pas avant</translation>
    </message>
    <message>
      <source>Not after</source>
      <translation>Pas après</translation>
    </message>
    <message>
      <source>Time range</source>
      <translation>Intervalle de temps</translation>
    </message>
    <message>
      <source>Local time</source>
      <translation>Heure locale</translation>
    </message>
    <message>
      <source>Days</source>
      <translation>Jours</translation>
    </message>
    <message>
      <source>Months</source>
      <translation>Mois</translation>
    </message>
    <message>
      <source>Years</source>
      <translation>Années</translation>
    </message>
    <message>
      <source>No well-defined expiration</source>
      <translation>Pas de date d'expiration précise</translation>
    </message>
    <message>
      <source>Midnight</source>
      <translation>Minuit</translation>
    </message>
    <message>
      <source>Apply</source>
      <translation>Appliquer</translation>
    </message>
    <message>
      <source>Revoke old certificate</source>
      <translation>Révoquer l'ancien certificat</translation>
    </message>
    <message>
      <source>The certificate will be earlier valid than the signer. This is probably not what you want.</source>
      <translation>Le certificat serait valide avant son signataire. Ce n'est probablement pas ce qui est désiré.</translation>
    </message>
    <message>
      <source>Edit dates</source>
      <translation>Modifier les limites de validité</translation>
    </message>
    <message>
      <source>Abort rollout</source>
      <translation>Interrompre le déploiement</translation>
    </message>
    <message>
      <source>Continue rollout</source>
      <translation>Continuer le déploiement</translation>
    </message>
    <message>
      <source>Adjust date and continue</source>
      <translation>Ajuster la date et continuer</translation>
    </message>
    <message>
      <source>The certificate will be longer valid than the signer. This is probably not what you want.</source>
      <translation>Le certificat serait valide plus longtemps que son signataire. Ce n'est probablement pas ce qui est désiré.</translation>
    </message>
  </context>
  <context>
    <name>CertTreeView</name>
    <message>
      <source>Import PKCS#12</source>
      <translation>Importer en PKCS#12</translation>
    </message>
    <message>
      <source>Import from PKCS#7</source>
      <translation>Importer en PKCS#7</translation>
    </message>
    <message>
      <source>Request</source>
      <translation>Requête</translation>
    </message>
    <message>
      <source>Security token</source>
      <translation>Jeton de sécurité</translation>
    </message>
    <message>
      <source>Other token</source>
      <translation>Autre jeton</translation>
    </message>
    <message>
      <source>Similar Certificate</source>
      <translation>Certificat similaire</translation>
    </message>
    <message>
      <source>Delete from Security token</source>
      <translation>Détruire sur le jeton de sécurité</translation>
    </message>
    <message>
      <source>CA</source>
      <translation>CA</translation>
    </message>
    <message>
      <source>Properties</source>
      <translation>Propriétés</translation>
    </message>
    <message>
      <source>Generate CRL</source>
      <translation>Générer la liste de révocation</translation>
    </message>
    <message>
      <source>Manage revocations</source>
      <translation>Gérer les révocations</translation>
    </message>
    <message>
      <source>Trust</source>
      <translation>Niveau de confiance</translation>
    </message>
    <message>
      <source>Renewal</source>
      <translation>Renouvellement</translation>
    </message>
    <message>
      <source>Revoke</source>
      <translation>Révoquer</translation>
    </message>
    <message>
      <source>Unrevoke</source>
      <translation>Dé-révoquer</translation>
    </message>
  </context>
  <context>
    <name>ClickLabel</name>
    <message>
      <source>Double click for details</source>
      <translation>Double-cliquer pour afficher les détails</translation>
    </message>
  </context>
  <context>
    <name>CrlDetail</name>
    <message>
      <source>Details of the Revocation list</source>
      <translation>Détails de la liste de révocation</translation>
    </message>
    <message>
      <source>&amp;Status</source>
      <translation>&amp;Etat</translation>
    </message>
    <message>
      <source>Version</source>
      <translation>Version</translation>
    </message>
    <message>
      <source>Signature</source>
      <translation>Signature</translation>
    </message>
    <message>
      <source>Signed by</source>
      <translation>Signé par</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>Nom</translation>
    </message>
    <message>
      <source>The internal name of the CRL in the database</source>
      <translation>Le nom interne de la liste de révocation dans la base de données</translation>
    </message>
    <message>
      <source>issuing dates</source>
      <translation>date d'émission</translation>
    </message>
    <message>
      <source>Next update</source>
      <translation>Prochaine mise-à-jour</translation>
    </message>
    <message>
      <source>Last update</source>
      <translation>Dernière mise-à-jour</translation>
    </message>
    <message>
      <source>&amp;Issuer</source>
      <translation>&amp;Emetteur</translation>
    </message>
    <message>
      <source>&amp;Extensions</source>
      <translation>E&amp;xtensions</translation>
    </message>
    <message>
      <source>&amp;Revocation list</source>
      <translation>Liste de &amp;révocation</translation>
    </message>
    <message>
      <source>Failed</source>
      <translation>Echoué</translation>
    </message>
    <message>
      <source>Unknown signer</source>
      <translation>Signataire inconnu</translation>
    </message>
    <message>
      <source>Verification not possible</source>
      <translation>Vérification impossible</translation>
    </message>
  </context>
  <context>
    <name>ExportDialog</name>
    <message>
      <source>Name</source>
      <translation>Nom</translation>
    </message>
    <message>
      <source>The internal name of the CRL in the database</source>
      <translation>Le nom interne de la liste de révocation dans la base de données</translation>
    </message>
    <message>
      <source>...</source>
      <translation>...</translation>
    </message>
    <message>
      <source>Filename</source>
      <translation>Nom du fichier</translation>
    </message>
    <message>
      <source>Export Format</source>
      <translation>Format d'exportation</translation>
    </message>
    <message>
      <source>All files ( * )</source>
      <translation>Tous les fichiers ( * )</translation>
    </message>
    <message>
      <source>PEM Text format with headers</source>
      <translation>Format textuel PEM avec des en-têtes</translation>
    </message>
    <message>
      <source>Concatenated list of all selected items in one PEM text file</source>
      <translation>Concaténation de tous les éléments sélectionnés dans un seul fichier texte en format PEM</translation>
    </message>
    <message>
      <source>Concatenated text format of the complete certificate chain in one PEM file</source>
      <translation>Fichier PEM contenant la concaténation de tous les certificats de la chaîne</translation>
    </message>
    <message>
      <source>Concatenated text format of all trusted certificates in one PEM file</source>
      <translation>Fichier PEM contenant la concaténation de tous les certificats de confiance</translation>
    </message>
    <message>
      <source>Concatenated text format of all certificates in one PEM file</source>
      <translation>Fichier PEM contenant la concaténation de tous les certificats</translation>
    </message>
    <message>
      <source>Binary DER encoded file</source>
      <translation>Fichier binaire encodé en DER</translation>
    </message>
    <message>
      <source>PKCS#7 encoded single certificate</source>
      <translation>Un seul certificat encodé en PKCS#7</translation>
    </message>
    <message>
      <source>PKCS#7 encoded complete certificate chain</source>
      <translation>La chaîne complète de certificats encodée en PKCS#7 dans un seul fichier</translation>
    </message>
    <message>
      <source>All trusted certificates encoded in one PKCS#7 file</source>
      <translation>Tous les certificats de confiance encodés en un seul fichier PKCS#7</translation>
    </message>
    <message>
      <source>All selected certificates encoded in one PKCS#7 file</source>
      <translation>Tous les certificats sélectionnés encodés en un seul fichier PKCS#7</translation>
    </message>
    <message>
      <source>All certificates encoded in one PKCS#7 file</source>
      <translation>Tous les certificats encodés en un seul fichier PKCS#7</translation>
    </message>
    <message>
      <source>The certificate and the private key as encrypted PKCS#12 file</source>
      <translation>Le certificat et sa clé privée encodés dans un fichier en format PKCS#12</translation>
    </message>
    <message>
      <source>The complete certificate chain and the private key as encrypted PKCS#12 file</source>
      <translation>La chaîne complète des certificats et la clé privée en un seul fichier PKCS#12</translation>
    </message>
    <message>
      <source>Concatenation of the certificate and the unencrypted private key in one PEM file</source>
      <translation>La concaténation du certificat et de sa clé privée non-cryptée dans un seul fichier PEM</translation>
    </message>
    <message>
      <source>Concatenation of the certificate and the encrypted private key in PKCS#8 format in one file</source>
      <translation>La concaténation du certificat et de sa clé privée encryptée en format PKCS#8 dans un seul fichier</translation>
    </message>
    <message>
      <source>Text format of the public key in one PEM file</source>
      <translation>Formattage textuel de la clé publique dans un fichier PEM</translation>
    </message>
    <message>
      <source>Binary DER format of the public key</source>
      <translation>Format binaire DER de la clé publique</translation>
    </message>
    <message>
      <source>Unencrypted private key in text format</source>
      <translation>Clé privée non-cryptée en format texte</translation>
    </message>
    <message>
      <source>OpenSSL specific encrypted private key in text format</source>
      <translation>Clé privée encryptée spécifiquement par OpenSSL en format texte</translation>
    </message>
    <message>
      <source>Unencrypted private key in binary DER format</source>
      <translation>Clé privée non-cryptée en format binaire DER</translation>
    </message>
    <message>
      <source>Unencrypted private key in PKCS#8 text format</source>
      <translation>Clé privée non-cryptée en format texte PKCS#8</translation>
    </message>
    <message>
      <source>Encrypted private key in PKCS#8 text format</source>
      <translation>Clé privée encryptée en format texte PKCS#8</translation>
    </message>
    <message>
      <source>The public key encoded in SSH2 format</source>
      <translation>La clé publique encodée en format SSH2</translation>
    </message>
    <message>
      <source>Certificate Index file</source>
      <translation>Ficher d'index des certificats</translation>
    </message>
    <message>
      <source>The file: '%1' already exists!</source>
      <translation>Le fichier '%1' existe déjà!</translation>
    </message>
    <message>
      <source>Overwrite</source>
      <translation>Ecraser</translation>
    </message>
    <message>
      <source>Do not overwrite</source>
      <translation>Ne pas écraser</translation>
    </message>
  </context>
  <context>
    <name>Help</name>
    <message>
      <source>&lt;&lt;</source>
      <translation>&lt;&lt;</translation>
    </message>
    <message>
      <source>&gt;&gt;</source>
      <translation>&lt;&lt;</translation>
    </message>
    <message>
      <source>&amp;Done</source>
      <translation>&amp;OK</translation>
    </message>
  </context>
  <context>
    <name>ImportMulti</name>
    <message>
      <source>Import PKI Items</source>
      <translation>Importer des objets PKI</translation>
    </message>
    <message>
      <source>Import &amp;All</source>
      <translation>Importer &amp;tout</translation>
    </message>
    <message>
      <source>&amp;Import</source>
      <translation>&amp;Importer</translation>
    </message>
    <message>
      <source>&amp;Done</source>
      <translation>&amp;OK</translation>
    </message>
    <message>
      <source>&amp;Remove from list</source>
      <translation>&amp;Retirer de la liste</translation>
    </message>
    <message>
      <source>Details</source>
      <translation>Détails</translation>
    </message>
    <message>
      <source>Delete from token</source>
      <translation>Enlever du jeton</translation>
    </message>
    <message>
      <source>Rename on token</source>
      <translation>Renommer dans le jeton</translation>
    </message>
    <message>
      <source>
Name: %1
Model: %2
Serial: %3</source>
      <translation>
Nom: %1
Modèle: %2
Numéro de série: %3</translation>
    </message>
    <message>
      <source>Manage security token</source>
      <translation>Gérer les jetons de sécurité</translation>
    </message>
    <message>
      <source>Details of the item '%1' cannot be shown</source>
      <translation>Les détails de l'objet '%1' ne peuvent pas être affichés</translation>
    </message>
    <message>
      <source>The type of the item '%1' is not recognized</source>
      <translation>Le type de l'objet '%1' n'est pas reconnu</translation>
    </message>
    <message>
      <source>The file '%1' did not contain PKI data</source>
      <translation>Le fichier '%1' ne contient pas de données PKI</translation>
    </message>
    <message>
      <source>The %1 files: '%2' did not contain PKI data</source>
      <translation>Les %1 fichiers: '%2' ne contiennent pas de données PKI</translation>
    </message>
  </context>
  <context>
    <name>KeyDetail</name>
    <message>
      <source>Name</source>
      <translation>Nom</translation>
    </message>
    <message>
      <source>The internal name of the key used by xca</source>
      <translation>Le nom interne de la clé utilisé par xca</translation>
    </message>
    <message>
      <source>Security token</source>
      <translation>Jeton de sécurité</translation>
    </message>
    <message>
      <source>Manufacturer</source>
      <translation>Fabricant</translation>
    </message>
    <message>
      <source>Serial</source>
      <translation>Numéro de série</translation>
    </message>
    <message>
      <source>Key</source>
      <translation>Clé</translation>
    </message>
    <message>
      <source>Public Exponent</source>
      <translation>Exposant public</translation>
    </message>
    <message>
      <source>Keysize</source>
      <translation>Taille de la clé</translation>
    </message>
    <message>
      <source>Private Exponent</source>
      <translation>Exposant privé</translation>
    </message>
    <message>
      <source>Modulus</source>
      <translation>Modulo</translation>
    </message>
    <message>
      <source>Details of the %1 key</source>
      <translation>Détails de la clé %1</translation>
    </message>
    <message>
      <source>Not available</source>
      <translation>Non disponible</translation>
    </message>
    <message>
      <source>Token</source>
      <translation>Jeton</translation>
    </message>
    <message>
      <source>Security token ID:%1</source>
      <translation>Identifiant de jeton de sécurité: %1</translation>
    </message>
    <message>
      <source>Available</source>
      <translation>Disponible</translation>
    </message>
    <message>
      <source>Sub prime</source>
      <translation>Sous-premier</translation>
    </message>
    <message>
      <source>Public key</source>
      <translation>Clé publique</translation>
    </message>
    <message>
      <source>Private key</source>
      <translation>Clé privée</translation>
    </message>
    <message>
      <source>Curve name</source>
      <translation>Nom de la courbe</translation>
    </message>
    <message>
      <source>Unknown key</source>
      <translation>Clé inconnue</translation>
    </message>
  </context>
  <context>
    <name>KeyTreeView</name>
    <message>
      <source>Change password</source>
      <translation>Changer le mot de passe</translation>
    </message>
    <message>
      <source>Reset password</source>
      <translation>Effacer le mot de passe</translation>
    </message>
    <message>
      <source>Change PIN</source>
      <translation>Changer le NIP</translation>
    </message>
    <message>
      <source>Init PIN with SO PIN (PUK)</source>
      <translation>Initialiser le NIP avec le PUK</translation>
    </message>
    <message>
      <source>Change SO PIN (PUK)</source>
      <translation>Changer le PUK</translation>
    </message>
    <message>
      <source>Security token</source>
      <translation>Jeton de sécurité</translation>
    </message>
    <message>
      <source>This is not a token</source>
      <translation>Ce n'est pas un jeton</translation>
    </message>
    <message>
      <source>Shall the original key '%1' be replaced by the key on the token?
This will delete the key '%1' and make it unexportable</source>
      <translation>La clé originale '%1' doit-elle être remplacée par la clé en provenance du jeton ?
Cela détruirait la clé '%1' et la rendrait non-exportable.</translation>
    </message>
  </context>
  <context>
    <name>MainWindow</name>
    <message>
      <source>Private Keys</source>
      <translation>Clés privées</translation>
    </message>
    <message>
      <source>&amp;New Key</source>
      <translation>&amp;Nouvelle clé</translation>
    </message>
    <message>
      <source>&amp;Export</source>
      <translation>&amp;Exporter</translation>
    </message>
    <message>
      <source>&amp;Import</source>
      <translation>&amp;Importer</translation>
    </message>
    <message>
      <source>Import PFX (PKCS#12)</source>
      <translation>Importer en PFX (PKCS#12)</translation>
    </message>
    <message>
      <source>&amp;Show Details</source>
      <translation>&amp;Afficher les Détails</translation>
    </message>
    <message>
      <source>&amp;Delete</source>
      <translation>&amp;Détruire</translation>
    </message>
    <message>
      <source>Certificate signing requests</source>
      <translation>Requêtes de signature de certificat</translation>
    </message>
    <message>
      <source>&amp;New Request</source>
      <translation>&amp;Nouvelle requête</translation>
    </message>
    <message>
      <source>Certificates</source>
      <translation>Certificats</translation>
    </message>
    <message>
      <source>&amp;New Certificate</source>
      <translation>&amp;Nouveau Certificat</translation>
    </message>
    <message>
      <source>Import &amp;PKCS#12</source>
      <translation>Importer en &amp;PKCS#12</translation>
    </message>
    <message>
      <source>Import P&amp;KCS#7</source>
      <translation>Importer en P&amp;KCS#7</translation>
    </message>
    <message>
      <source>Plain View</source>
      <translation>Vue à plat</translation>
    </message>
    <message>
      <source>Templates</source>
      <translation>Modèles</translation>
    </message>
    <message>
      <source>&amp;New Template</source>
      <translation>&amp;Nouveau modèle</translation>
    </message>
    <message>
      <source>&amp;New CRL</source>
      <translation>&amp;Nouvelle liste de révocation</translation>
    </message>
    <message>
      <source>Ch&amp;ange Template</source>
      <translation>&amp;Modifier le Modèle</translation>
    </message>
    <message>
      <source>Revocation lists</source>
      <translation>Listes de révocation</translation>
    </message>
    <message>
      <source>Using or exporting private keys will not be possible without providing the correct password</source>
      <translation>L'usage ou l'exportation de clés privées seront impossible sans fournir le mot de passe correct.</translation>
    </message>
    <message>
      <source>Database</source>
      <translation>Base de donnée</translation>
    </message>
    <message>
      <source>The currently used default hash '%1' is insecure. Please select at least 'SHA 224' for security reasons.</source>
      <translation>L'algorithme de hachage '%1' sélectionné n'est pas sûr. SVP choisissez au moins 'SHA 224' pour raisons de sécurité.</translation>
    </message>
    <message>
      <source>No deleted items found</source>
      <translation>Aucun objet détruit n'a été trouvé</translation>
    </message>
    <message>
      <source>Errors detected and repaired while deleting outdated items from the database. A backup file was created</source>
      <translation>Des erreurs ont été détectées et réparées lors de la destruction des éléments échus de la base de données. Un fichier de sauvegarde a été créé</translation>
    </message>
    <message>
      <source>Removing deleted or outdated items from the database failed.</source>
      <translation>Le nettoyage des éléments détruits ou échus de la base de données a échoué.</translation>
    </message>
    <message>
      <source>Recent DataBases</source>
      <translation>Bases de données utilisées recemment</translation>
    </message>
    <message>
      <source>System</source>
      <translation>Système</translation>
    </message>
    <message>
      <source>Croatian</source>
      <translation>Croate</translation>
    </message>
    <message>
      <source>English</source>
      <translation>Anglais</translation>
    </message>
    <message>
      <source>French</source>
      <translation>Français</translation>
    </message>
    <message>
      <source>German</source>
      <translation>Allemand</translation>
    </message>
    <message>
      <source>Russian</source>
      <translation>Russe</translation>
    </message>
    <message>
      <source>Slovak</source>
      <translation>Slovaque</translation>
    </message>
    <message>
      <source>Spanish</source>
      <translation>Espagnol</translation>
    </message>
    <message>
      <source>Turkish</source>
      <translation>Turc</translation>
    </message>
    <message>
      <source>Language</source>
      <translation>Langue</translation>
    </message>
    <message>
      <source>&amp;File</source>
      <translation>&amp;Fichier</translation>
    </message>
    <message>
      <source>&amp;New DataBase</source>
      <translation>&amp;Nouvelle base de données</translation>
    </message>
    <message>
      <source>&amp;Open DataBase</source>
      <translation>&amp;Ouvrir une base de données</translation>
    </message>
    <message>
      <source>Set as default DataBase</source>
      <translation>Définir comme base de données par défaut</translation>
    </message>
    <message>
      <source>&amp;Close DataBase</source>
      <translation>&amp;Fermer la base de données</translation>
    </message>
    <message>
      <source>Options</source>
      <translation>Options</translation>
    </message>
    <message>
      <source>Exit</source>
      <translation>Quitter</translation>
    </message>
    <message>
      <source>I&amp;mport</source>
      <translation>I&amp;mporter</translation>
    </message>
    <message>
      <source>Keys</source>
      <translation>Clés</translation>
    </message>
    <message>
      <source>Requests</source>
      <translation>Requêtes</translation>
    </message>
    <message>
      <source>PKCS#12</source>
      <translation>PKCS#12</translation>
    </message>
    <message>
      <source>PKCS#7</source>
      <translation>PKCS#7</translation>
    </message>
    <message>
      <source>Template</source>
      <translation>Modèle</translation>
    </message>
    <message>
      <source>Revocation list</source>
      <translation>Liste de révocation</translation>
    </message>
    <message>
      <source>PEM file</source>
      <translation>Fichier PEM</translation>
    </message>
    <message>
      <source>Paste PEM file</source>
      <translation>Coller un fichier PEM</translation>
    </message>
    <message>
      <source>Database dump ( *.dump );; All files ( * )</source>
      <translation>Cliché de base de données ( *.dump );;Tous les fichiers ( * )</translation>
    </message>
    <message>
      <source>&amp;Token</source>
      <translation>&amp;Jeton</translation>
    </message>
    <message>
      <source>&amp;Manage Security token</source>
      <translation>&amp;Gérer le jeton de sécurité</translation>
    </message>
    <message>
      <source>&amp;Init Security token</source>
      <translation>&amp;Initialiser le jeton de sécurité</translation>
    </message>
    <message>
      <source>&amp;Change PIN</source>
      <translation>&amp;Changer le NIP</translation>
    </message>
    <message>
      <source>Change &amp;SO PIN</source>
      <translation>Changer le P&amp;UK</translation>
    </message>
    <message>
      <source>Init PIN</source>
      <translation>Initialiser le NIP</translation>
    </message>
    <message>
      <source>Extra</source>
      <translation>Extra</translation>
    </message>
    <message>
      <source>&amp;Dump DataBase</source>
      <translation>C&amp;licher la base de données</translation>
    </message>
    <message>
      <source>&amp;Export Certificate Index</source>
      <translation>Exporter l'index des certificats</translation>
    </message>
    <message>
      <source>&amp;Export Certificate Index hierarchy</source>
      <translation>&amp;Exporter la hiérarchie de l'index des certificats</translation>
    </message>
    <message>
      <source>C&amp;hange DataBase password</source>
      <translation>C&amp;hanger le mot de passe de la base de données</translation>
    </message>
    <message>
      <source>&amp;Import old db_dump</source>
      <translation>&amp;Importer un cliché de base de données en ancien format (db_dump)</translation>
    </message>
    <message>
      <source>&amp;Undelete items</source>
      <translation>&amp;Récupérer des objets détruits</translation>
    </message>
    <message>
      <source>Generate DH parameter</source>
      <translation>Générer le paramètre DH</translation>
    </message>
    <message>
      <source>OID Resolver</source>
      <translation>Convertisseur d'OID</translation>
    </message>
    <message>
      <source>&amp;Help</source>
      <translation>&amp;Aide</translation>
    </message>
    <message>
      <source>&amp;Content</source>
      <translation>&amp;Contenu</translation>
    </message>
    <message>
      <source>About</source>
      <translation>A propos</translation>
    </message>
    <message>
      <source>Import password</source>
      <translation>Mot de passe d'importation</translation>
    </message>
    <message>
      <source>Please enter the password of the old database</source>
      <translation>SVP saisir le mot de passe de l'ancienne base de données</translation>
    </message>
    <message>
      <source>Password verification error. Ignore keys ?</source>
      <translation>La vérification du mot de passe a écoué. Ignorer les clés ?</translation>
    </message>
    <message>
      <source>Import anyway</source>
      <translation>Importer quand-même</translation>
    </message>
    <message>
      <source>no such option: %1</source>
      <translation>'%1' n'est pas une option</translation>
    </message>
    <message>
      <source>Import PEM data</source>
      <translation>Importer les données PEM</translation>
    </message>
    <message>
      <source>Please enter the original SO PIN (PUK) of the token '%1'</source>
      <translation>SVP saisir le PUK original du jeton '%1'</translation>
    </message>
    <message>
      <source>Search</source>
      <translation>Chercher</translation>
    </message>
    <message>
      <source>Please enter the new SO PIN (PUK) for the token '%1'</source>
      <translation>SVP saisir le nouveau NIP SO (PUK) pour le jeton: '%1'</translation>
    </message>
    <message>
      <source>The new label of the token '%1'</source>
      <translation>La nouvelle étiquette du jeton '%1'</translation>
    </message>
    <message>
      <source>The token '%1' did not contain any keys or certificates</source>
      <translation>Le jeton '%1' ne contient aucune clé ni aucun certificat</translation>
    </message>
    <message>
      <source>Current Password</source>
      <translation>Mot de passe actuel</translation>
    </message>
    <message>
      <source>Please enter the current database password</source>
      <translation>SVP saisir le mot de passe de la base de données</translation>
    </message>
    <message>
      <source>The entered password is wrong</source>
      <translation>Le mot de passe renseigné est inexact</translation>
    </message>
    <message>
      <source>New Password</source>
      <translation>Nouveau mot de passe</translation>
    </message>
    <message>
      <source>Please enter the new password to encrypt your private keys in the database-file</source>
      <translation>SVP saisir le nouveau mot de passe pour encrypter les clés privées dans le fichier de base de données</translation>
    </message>
    <message>
      <source>Please enter a password, that will be used to encrypt your private keys in the database file:
%1</source>
      <translation>Veuillez entrer un mot de passe, qui sera utiliser pour chiffrer vos clés privées dans le fichier de la base de données:
%1</translation>
    </message>
    <message>
      <source>Password verify error, please try again</source>
      <translation>La vérification du mot de passe a échoué. SVP essayez encore</translation>
    </message>
    <message>
      <source>Password</source>
      <translation>Mot de passe</translation>
    </message>
    <message>
      <source>Please enter the password for unlocking the database:
%1</source>
      <translation>Veuillez entrer le mot passe pour déverrouiller la base de données:
%1</translation>
    </message>
    <message>
      <source>The following error occurred:</source>
      <translation>L'erreur suivante s'est produite:</translation>
    </message>
    <message>
      <source>Copy to Clipboard</source>
      <translation>Copier dans le presse-papier</translation>
    </message>
    <message>
      <source>Certificate Index ( index.txt )</source>
      <translation>Index des certificats ( index.txt )</translation>
    </message>
    <message>
      <source>All files ( * )</source>
      <translation>Tous les fichiers ( * )</translation>
    </message>
    <message>
      <source>Diffie-Hellman parameters are needed for different applications, but not handled by XCA.
Please enter the DH parameter bits</source>
      <translation>Les paramètres de Diffie-Hellman sont nécessaires à certaines applications mais ne sont pas gérés par XCA.
Saisir le nombre de bits du paramètre de Diffie-Hellman SVP</translation>
    </message>
    <message>
      <source>Error opening file: '%1': %2</source>
      <translation>Erreur lors de l'ouverture du fichier: '%1': %2</translation>
    </message>
  </context>
  <context>
    <name>NewCrl</name>
    <message>
      <source>Create CRL</source>
      <translation>Créer une liste de révocation</translation>
    </message>
    <message>
      <source>Last update</source>
      <translation>Dernière mise-à-jour</translation>
    </message>
    <message>
      <source>Next update</source>
      <translation>Prochaine mise-à-jour</translation>
    </message>
    <message>
      <source>Days</source>
      <translation>Jours</translation>
    </message>
    <message>
      <source>Months</source>
      <translation>Mois</translation>
    </message>
    <message>
      <source>Years</source>
      <translation>Années</translation>
    </message>
    <message>
      <source>Midnight</source>
      <translation>Minuit</translation>
    </message>
    <message>
      <source>Local time</source>
      <translation>Heure locale</translation>
    </message>
    <message>
      <source>Apply</source>
      <translation>Appliquer</translation>
    </message>
    <message>
      <source>Options</source>
      <translation>Options</translation>
    </message>
    <message>
      <source>CRL number</source>
      <translation>Numéro de la liste de révocation</translation>
    </message>
    <message>
      <source>Subject alternative name</source>
      <translation>Nom alternatif du sujet</translation>
    </message>
    <message>
      <source>Revocation reasons</source>
      <translation>Raisons de la révocation</translation>
    </message>
    <message>
      <source>Authority key identifier</source>
      <translation>Identifiant de clé de l'autorité</translation>
    </message>
    <message>
      <source>Hash algorithm</source>
      <translation>Algorithme de hachage</translation>
    </message>
  </context>
  <context>
    <name>NewKey</name>
    <message>
      <source>Please give a name to the new key and select the desired keysize</source>
      <translation>SVP donner un nom à la nouvelle clé et sélectionner la taille de clé désirée</translation>
    </message>
    <message>
      <source>Key properties</source>
      <translation>Propriétés de la clé</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>Nom</translation>
    </message>
    <message>
      <source>The internal name of the new key</source>
      <translation>Le nom interne de la nouvelle clé</translation>
    </message>
    <message>
      <source>Curve name</source>
      <translation>Nom de la courbe</translation>
    </message>
    <message>
      <source>Usually at least 2048 bit keys are recommended</source>
      <translation>Généralement une clé de 2048 bits est recommandée</translation>
    </message>
    <message>
      <source>New Key</source>
      <translation>Nouvelle Clé</translation>
    </message>
    <message>
      <source>Keysize</source>
      <translation>Taille de la clé</translation>
    </message>
    <message>
      <source>Keytype</source>
      <translation>Type de clé</translation>
    </message>
    <message>
      <source>Remember as default</source>
      <translation>Enregistrer comme valeur par défaut</translation>
    </message>
    <message>
      <source>Create</source>
      <translation>Créer</translation>
    </message>
  </context>
  <context>
    <name>NewX509</name>
    <message>
      <source>Source</source>
      <translation>Source</translation>
    </message>
    <message>
      <source>Signing request</source>
      <translation>Requête de signature</translation>
    </message>
    <message>
      <source>Show request</source>
      <translation>Afficher la requête</translation>
    </message>
    <message>
      <source>Sign this Certificate signing &amp;request</source>
      <translation>Signer cette &amp;requête</translation>
    </message>
    <message>
      <source>Copy extensions from the request</source>
      <translation>Copier les extensions de la requête</translation>
    </message>
    <message>
      <source>Modify subject of the request</source>
      <translation>Modifier le sujet de la requête</translation>
    </message>
    <message>
      <source>Signing</source>
      <translation>Signer</translation>
    </message>
    <message>
      <source>Create a &amp;self signed certificate with the serial</source>
      <translation>Créer un certificat auto-&amp;signé avec le numéro de série</translation>
    </message>
    <message>
      <source>If you leave this blank the serial 00 will be used</source>
      <translation>Si ce champ est laissé vide, le numéro de série 00 sera utilisé</translation>
    </message>
    <message>
      <source>1</source>
      <translation>1</translation>
    </message>
    <message>
      <source>Use &amp;this Certificate for signing</source>
      <translation>Utiliser &amp;ce certificat pour signer</translation>
    </message>
    <message>
      <source>All certificates in your database that can create valid signatures</source>
      <translation>Tous les certificats dans la base de données qui peuvent produire des signatures valables</translation>
    </message>
    <message>
      <source>Signature algorithm</source>
      <translation>Algorithme de signature</translation>
    </message>
    <message>
      <source>Template for the new certificate</source>
      <translation>Modèle pour le nouveau certificat</translation>
    </message>
    <message>
      <source>All available templates</source>
      <translation>Tous les modèles disponibles</translation>
    </message>
    <message>
      <source>Apply extensions</source>
      <translation>Appliquer les extensions</translation>
    </message>
    <message>
      <source>Apply subject</source>
      <translation>Appliquer le sujet</translation>
    </message>
    <message>
      <source>Apply all</source>
      <translation>Appliquer tout</translation>
    </message>
    <message>
      <source>Subject</source>
      <translation>Sujet</translation>
    </message>
    <message>
      <source>Distinguished name</source>
      <translation>Nom distinctif</translation>
    </message>
    <message>
      <source>Add</source>
      <translation>Ajouter</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Enlever</translation>
    </message>
    <message>
      <source>Private key</source>
      <translation>Clé privée</translation>
    </message>
    <message>
      <source>This list only contains unused keys</source>
      <translation>Cette liste ne contient que les clés inutilisées</translation>
    </message>
    <message>
      <source>Used keys too</source>
      <translation>Inclure les clés utilisées</translation>
    </message>
    <message>
      <source>&amp;Generate a new key</source>
      <translation>&amp;Générer une nouvelle clé</translation>
    </message>
    <message>
      <source>Extensions</source>
      <translation>Extensions</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Type</translation>
    </message>
    <message>
      <source>If this will become a CA certificate or not</source>
      <translation>Si un certificat d'autorité (CA) est en train d'être créé ou non</translation>
    </message>
    <message>
      <source>Not defined</source>
      <translation>Non défini</translation>
    </message>
    <message>
      <source>Certification Authority</source>
      <translation>Autorité de Certification</translation>
    </message>
    <message>
      <source>End Entity</source>
      <translation>Entité Finale</translation>
    </message>
    <message>
      <source>Path length</source>
      <translation>Distance aux feuilles</translation>
    </message>
    <message>
      <source>How much CAs may be below this.</source>
      <translation>Combien de niveau de sous-CA peuvent apparaître jusqu'à une entité finale.</translation>
    </message>
    <message>
      <source>The basic constraints should always be critical</source>
      <translation>Les contraintes basiques doivent toujours être critiques</translation>
    </message>
    <message>
      <source>Key identifier</source>
      <translation>Identifiant de clé</translation>
    </message>
    <message>
      <source>Creates a hash of the key following the PKIX guidelines</source>
      <translation>Crée un hachage de la clé conformément aux directives PKIX</translation>
    </message>
    <message>
      <source>Copy the Subject Key Identifier from the issuer</source>
      <translation>Copie l'identifiant de clé du sujet du signataire</translation>
    </message>
    <message>
      <source>Validity</source>
      <translation>Validité</translation>
    </message>
    <message>
      <source>Not before</source>
      <translation>Pas avant</translation>
    </message>
    <message>
      <source>Not after</source>
      <translation>Pas après</translation>
    </message>
    <message>
      <source>Time range</source>
      <translation>Intervalle de temps</translation>
    </message>
    <message>
      <source>Days</source>
      <translation>Jours</translation>
    </message>
    <message>
      <source>Months</source>
      <translation>Mois</translation>
    </message>
    <message>
      <source>Years</source>
      <translation>Années</translation>
    </message>
    <message>
      <source>Apply</source>
      <translation>Appliquer</translation>
    </message>
    <message>
      <source>Set the time to 00:00:00 and 23:59:59 respectively</source>
      <translation>Définir les heures à 00:00:00 et 23:59:59 respectivement</translation>
    </message>
    <message>
      <source>Midnight</source>
      <translation>Minuit</translation>
    </message>
    <message>
      <source>Local time</source>
      <translation>Heure locale</translation>
    </message>
    <message>
      <source>No well-defined expiration</source>
      <translation>Pas de date d'expiration précise</translation>
    </message>
    <message>
      <source>DNS: IP: URI: email: RID:</source>
      <translation>DNS: IP: URI: email: RID:</translation>
    </message>
    <message>
      <source>Edit</source>
      <translation>Modifier</translation>
    </message>
    <message>
      <source>URI:</source>
      <translation>URI:</translation>
    </message>
    <message>
      <source>can be altered by the file "aia.txt"</source>
      <translation>peut être altéré par le fichier "aia.txt"</translation>
    </message>
    <message>
      <source>Key usage</source>
      <translation>Utilisation de la clé</translation>
    </message>
    <message>
      <source>Netscape</source>
      <translation>Netscape</translation>
    </message>
    <message>
      <source>Advanced</source>
      <translation>Avancé</translation>
    </message>
    <message>
      <source>Validate</source>
      <translation>Valider</translation>
    </message>
    <message>
      <source>Create a &amp;self signed certificate with a MD5-hashed QA serial</source>
      <translation>Créer un certificat auto-&amp;signé avec un numéro de série QA basé sur MD5</translation>
    </message>
    <message>
      <source>This name is only used internally and does not appear in the resulting certificate</source>
      <translation>Ce nom est seulement utilisé par xca et n'apparaît pas dans le certificat exporté</translation>
    </message>
    <message>
      <source>Internal name</source>
      <translation>Nom interne</translation>
    </message>
    <message>
      <source>Critical</source>
      <translation>Critique</translation>
    </message>
    <message>
      <source>Create Certificate signing request</source>
      <translation>Créer une requête de signature de certificat</translation>
    </message>
    <message>
      <source>minimum size: %1</source>
      <translation>taille minimale: %1</translation>
    </message>
    <message>
      <source>maximum size: %1</source>
      <translation>taille maximale: %1</translation>
    </message>
    <message>
      <source>only a-z A-Z 0-9 '()+,-./:=?</source>
      <translation>seulement a-z A-Z 0-9 '()+,-./:=?</translation>
    </message>
    <message>
      <source>only 7-bit clean characters</source>
      <translation>seulement des caractères 7-bit ASCII imprimables</translation>
    </message>
    <message>
      <source>Create XCA template</source>
      <translation>Créer un modèle XCA</translation>
    </message>
    <message>
      <source>Edit XCA template</source>
      <translation>Editer un modèle XCA</translation>
    </message>
    <message>
      <source>Create x509 Certificate</source>
      <translation>Créer un certificat x509</translation>
    </message>
    <message>
      <source>Other Tabs</source>
      <translation>Autres Onglets</translation>
    </message>
    <message>
      <source>Advanced Tab</source>
      <translation>Onglet Avancé</translation>
    </message>
    <message>
      <source>Errors</source>
      <translation>Erreurs</translation>
    </message>
    <message>
      <source>From PKCS#10 request</source>
      <translation>A partir d'une requête en PKCS#10</translation>
    </message>
    <message>
      <source>Abort rollout</source>
      <translation>Interrompre le déploiement</translation>
    </message>
    <message>
      <source>The following length restrictions of RFC3280 are violated:</source>
      <translation>Pour être en conformité avec la RFC3280, les règles de restriction de longueur suivantes ne sont pas respectées:</translation>
    </message>
    <message>
      <source>Edit subject</source>
      <translation>Modifier le sujet</translation>
    </message>
    <message>
      <source>Continue rollout</source>
      <translation>Continuer le déploiement</translation>
    </message>
    <message>
      <source>The verification of the Certificate request failed.
The rollout should be aborted.</source>
      <translation>La vérification de la requête de signature à échoué.
Le déploiement devrait être interrompu.</translation>
    </message>
    <message>
      <source>Continue anyway</source>
      <translation>Continuer quand-même</translation>
    </message>
    <message>
      <source>The internal name and the common name are empty.
Please set at least the internal name.</source>
      <translation>Le nom interne et le nom commun sont vides.
SVP saisir au moins le nom interne.</translation>
    </message>
    <message>
      <source>Edit name</source>
      <translation>Modifier le nom</translation>
    </message>
    <message>
      <source>There is no Key selected for signing.</source>
      <translation>Aucune clé n'est sélectionnée pour la signature.</translation>
    </message>
    <message>
      <source>Select key</source>
      <translation>Sélectionner la clé</translation>
    </message>
    <message>
      <source>The following distinguished name entries are empty:
%1
though you have declared them as mandatory in the options menu.</source>
      <translation>Les indications de nom distinctif suivantes sont vides:
%1
bien que vous les avez déclarées comme obligatoires dans le menu des options.</translation>
    </message>
    <message>
      <source>The key you selected for signing is not a private one.</source>
      <translation>La clé sélectionnée pour la signature n'est pas une clé privée.</translation>
    </message>
    <message>
      <source>Select other signer</source>
      <translation>Sélectionner un autre signataire</translation>
    </message>
    <message>
      <source>Select other key</source>
      <translation>Sélectionner une autre clé</translation>
    </message>
    <message>
      <source>The certificate will be earlier valid than the signer. This is probably not what you want.</source>
      <translation>Le certificat serait valide avant son signataire. Ce n'est probablement pas ce qui est désiré.
</translation>
    </message>
    <message>
      <source>Edit dates</source>
      <translation>Modifier les limites de validité</translation>
    </message>
    <message>
      <source>Adjust date and continue</source>
      <translation>Ajuster la date et continuer</translation>
    </message>
    <message>
      <source>The certificate will be longer valid than the signer. This is probably not what you want.</source>
      <translation>Le certificat serait valide plus longtemps que son signataire. Ce n'est probablement pas ce qui est désiré.</translation>
    </message>
    <message>
      <source>The certificate will be out of date before it becomes valid. You most probably mixed up both dates.</source>
      <translation>Le certificat serait échu avant de devenir actif. Vous avez probablement interverti les deux dates.</translation>
    </message>
    <message>
      <source>The certificate contains invalid or duplicate extensions. Check the validation on the advanced tab.</source>
      <translation>Le certificat contient des extensions dupliquées ou invalides. Vérifier la validité sur l'onglet "Avancé".</translation>
    </message>
    <message>
      <source>Edit extensions</source>
      <translation>Modifier les extensions</translation>
    </message>
    <message>
      <source>Configfile error on line %1
</source>
      <translation>Erreur dans le fichier de configuration à la ligne %1
</translation>
    </message>
  </context>
  <context>
    <name>OidResolver</name>
    <message>
      <source>OID Resolver</source>
      <translation>Convertisseur d'OID</translation>
    </message>
    <message>
      <source>Enter the OID, the Nid, or one of the textual representations</source>
      <translation>Saisir l'OID, le Nid ou une des représentations symboliques</translation>
    </message>
    <message>
      <source>Search</source>
      <translation>Chercher</translation>
    </message>
    <message>
      <source>OID</source>
      <translation>OID</translation>
    </message>
    <message>
      <source>Long name</source>
      <translation>Nom complet</translation>
    </message>
    <message>
      <source>OpenSSL internal ID</source>
      <translation>Identification interne à OpenSSL</translation>
    </message>
    <message>
      <source>Nid</source>
      <translation>Nid</translation>
    </message>
    <message>
      <source>Short name</source>
      <translation>Nom abrégé</translation>
    </message>
  </context>
  <context>
    <name>Options</name>
    <message>
      <source>XCA Options</source>
      <translation>Options de XCA</translation>
    </message>
    <message>
      <source>Settings</source>
      <translation>Réglages</translation>
    </message>
    <message>
      <source>Default hash algorithm</source>
      <translation>Algorithme de hachage par défaut</translation>
    </message>
    <message>
      <source>String types</source>
      <translation>Types de chaînes</translation>
    </message>
    <message>
      <source>Suppress success messages</source>
      <translation>Supprimer les messages en cas de succès</translation>
    </message>
    <message>
      <source>Don't colorize expired certificates</source>
      <translation>Ne pas coloriser les certificats expirés</translation>
    </message>
    <message>
      <source>Translate established x509 terms (%1 -&gt; %2)</source>
      <translation>Traduire les termes standards définis par x509 (%1 -&gt; %2)</translation>
    </message>
    <message>
      <source>The hashing functionality of the token is not used by XCA.
It may however honor a restricted hash-set propagated by the token.
Especially EC and DSA are only defined with SHA1 in the PKCS#11 specification.</source>
      <translation>La fonctionalité de hachage du jeton n'est pas utilisée par XCA.
Ce dernier peut néanmoins se limiter à un jeu restreint de fonctions de hachage reconnus par le jeton.
En particulier, EC et DSA ne sont définis qu'avec SHA1 dans la spécification PKCS#11.</translation>
    </message>
    <message>
      <source>Only use hashes supported by the token when signing with a token key</source>
      <translation>N'utiliser que des fonction de hachage supportées par le jeton lors d'une signature avec une clé du jeton</translation>
    </message>
    <message>
      <source>Disable legacy Netscape extensions</source>
      <translation>Désactiver les extensions Netscape désuètes</translation>
    </message>
    <message>
      <source>Distinguished name</source>
      <translation>Nom distinctif</translation>
    </message>
    <message>
      <source>Mandatory subject entries</source>
      <translation>Inscriptions du sujet obligatoires</translation>
    </message>
    <message>
      <source>Add</source>
      <translation>Ajouter</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Enlever</translation>
    </message>
    <message>
      <source>Explicit subject entries</source>
      <translation>Inscriptions explicites du sujet</translation>
    </message>
    <message>
      <source>Default</source>
      <translation>Défaut</translation>
    </message>
    <message>
      <source>PKCS#11 provider</source>
      <translation>Fournisseur PKCS#11</translation>
    </message>
    <message>
      <source>Remove</source>
      <translation>Enlever</translation>
    </message>
    <message>
      <source>Search</source>
      <translation>Chercher</translation>
    </message>
    <message>
      <source>Printable string or UTF8 (default)</source>
      <translation>Chaîne imprimable ou UTF8 (défaut)</translation>
    </message>
    <message>
      <source>PKIX recommendation in RFC2459</source>
      <translation>La recommandation PKIX est dans la RFC2459</translation>
    </message>
    <message>
      <source>No BMP strings, only printable and T61</source>
      <translation>Pas de chaîne BMP, seulement imprimable et T61</translation>
    </message>
    <message>
      <source>UTF8 strings only (RFC2459)</source>
      <translation>Chaînes UTF8 uniquement (RFC2459)</translation>
    </message>
    <message>
      <source>All strings</source>
      <translation>Toutes les chaînes</translation>
    </message>
    <message>
      <source>Load failed</source>
      <translation>Le chargement a échoué</translation>
    </message>
  </context>
  <context>
    <name>PwDialog</name>
    <message>
      <source>The password is parsed as 2-digit hex code. It must have an even number of digits (0-9 and a-f)</source>
      <translation>Le mot de passe doit être exprimé sous forme d'une suite de chiffres hexadécimaux. Il doit contenir un nombre pair de chiffres (0-9 et a-f)</translation>
    </message>
    <message>
      <source>Take as HEX string</source>
      <translation>Exprimé en hexadécimal</translation>
    </message>
    <message>
      <source>Repeat %1</source>
      <translation>Répéter %1</translation>
    </message>
    <message>
      <source>%1 mismatch</source>
      <translation>%1 ne correspond pas</translation>
    </message>
    <message>
      <source>Hex password must only contain the characters '0' - '9' and 'a' - 'f' and it must consist of an even number of characters</source>
      <translation>Un mot de passe hexadécimal peut seulement contenir les caractères '0' à '9' et 'a' à 'f' et il doit consister en un nombre pair de caractères</translation>
    </message>
    <message>
      <source>E&amp;xit</source>
      <translation>&amp;Quitter</translation>
    </message>
  </context>
  <context>
    <name>QObject</name>
    <message>
      <source>Undefined</source>
      <translation>Indéfini</translation>
    </message>
    <message>
      <source>Broken / Invalid</source>
      <translation>Invalide</translation>
    </message>
    <message>
      <source>DB: Rename: '%1' already in use</source>
      <translation>DB: Renommer: '%1' déjà en utilisation</translation>
    </message>
    <message>
      <source>DB: Entry to rename not found: %1</source>
      <translation>DB: l'inscription à renommer n'a pas été trouvée: '%1'</translation>
    </message>
    <message>
      <source>DB: Write error %1 - %2</source>
      <translation>DB: Erreur d'écriture %1 - %2</translation>
    </message>
    <message>
      <source>Out of data</source>
      <translation>A court de données</translation>
    </message>
    <message>
      <source>Error finding endmarker of string</source>
      <translation>Erreur: une marque de fin de chaîne n'a pas été trouvée</translation>
    </message>
    <message>
      <source>Out of Memory at %1:%2</source>
      <translation>A court de mémoire à %1:%2</translation>
    </message>
    <message>
      <source>Country code</source>
      <translation>Code du pays</translation>
    </message>
    <message>
      <source>State or Province</source>
      <translation>Etat ou Province</translation>
    </message>
    <message>
      <source>Locality</source>
      <translation>Ville</translation>
    </message>
    <message>
      <source>Organisation</source>
      <translation>Organisation</translation>
    </message>
    <message>
      <source>Organisational unit</source>
      <translation>Unité organisationnelle</translation>
    </message>
    <message>
      <source>Common name</source>
      <translation>Nom commun</translation>
    </message>
    <message>
      <source>E-Mail address</source>
      <translation>Adresse de courriel</translation>
    </message>
    <message>
      <source>Serial number</source>
      <translation>Numéro de série</translation>
    </message>
    <message>
      <source>Given name</source>
      <translation>Prénom</translation>
    </message>
    <message>
      <source>Surname</source>
      <translation>Nom de famille</translation>
    </message>
    <message>
      <source>Title</source>
      <translation>Titre</translation>
    </message>
    <message>
      <source>Initials</source>
      <translation>Initiales</translation>
    </message>
    <message>
      <source>Description</source>
      <translation>Description</translation>
    </message>
    <message>
      <source>Role</source>
      <translation>Rôle</translation>
    </message>
    <message>
      <source>Pseudonym</source>
      <translation>Pseudonyme</translation>
    </message>
    <message>
      <source>Generation Qualifier</source>
      <translation>Qualificatif de génération</translation>
    </message>
    <message>
      <source>x500 Unique Identifier</source>
      <translation>Identifiant unique X500</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>Nom</translation>
    </message>
    <message>
      <source>DN Qualifier</source>
      <translation>Qualificatif de DN</translation>
    </message>
    <message>
      <source>Unstructured name</source>
      <translation>Nom non-structuré</translation>
    </message>
    <message>
      <source>Challenge password</source>
      <translation>Mot de passe challenge</translation>
    </message>
    <message>
      <source>Basic Constraints</source>
      <translation>Contraintes basiques</translation>
    </message>
    <message>
      <source>Subject alternative name</source>
      <translation>Nom alternatif du sujet</translation>
    </message>
    <message>
      <source>issuer alternative name</source>
      <translation>Nom alternatif du signataire</translation>
    </message>
    <message>
      <source>Subject key identifier</source>
      <translation>Identifiant de clé du sujet</translation>
    </message>
    <message>
      <source>Authority key identifier</source>
      <translation>Identifiant de clé de l'autorité</translation>
    </message>
    <message>
      <source>Key usage</source>
      <translation>Utilisation de la clé</translation>
    </message>
    <message>
      <source>Extended key usage</source>
      <translation>Utilisation étendue de la clé</translation>
    </message>
    <message>
      <source>CRL distribution points</source>
      <translation>Points de distribution de la liste de révocation</translation>
    </message>
    <message>
      <source>Authority information access</source>
      <translation>Accès à l'information de l'autorité</translation>
    </message>
    <message>
      <source>Certificate type</source>
      <translation>Type de certificat</translation>
    </message>
    <message>
      <source>Base URL</source>
      <translation>URL de base</translation>
    </message>
    <message>
      <source>Revocation URL</source>
      <translation>URL de révocation</translation>
    </message>
    <message>
      <source>CA Revocation URL</source>
      <translation>URL de révocation du certificat CA</translation>
    </message>
    <message>
      <source>Certificate renewal URL</source>
      <translation>URL de renouvellement de certificat</translation>
    </message>
    <message>
      <source>CA policy URL</source>
      <translation>URL des conditions générales du CA</translation>
    </message>
    <message>
      <source>SSL server name</source>
      <translation>Nom du serveur SSL</translation>
    </message>
    <message>
      <source>Comment</source>
      <translation>Commentaire</translation>
    </message>
    <message>
      <source>All files ( * )</source>
      <translation>Tous les fichiers ( * )</translation>
    </message>
    <message>
      <source>Import RSA key</source>
      <translation>Importer une clé RSA</translation>
    </message>
    <message>
      <source>PKI Keys ( *.pem *.der *.key );; PKCS#8 Keys ( *.p8 *.pk8 );; SSH Public Keys ( *.pub );;</source>
      <translation>Clés PKI ( *.pem *.der *.key );;Clés PKCS#8 ( *.p8 *.pk8 );;Clés publiques SSH ( *.pub );;</translation>
    </message>
    <message>
      <source>PKCS#10 CSR ( *.pem *.der *.csr );; Netscape Request ( *.spkac *.spc );;</source>
      <translation>Requête PKCS#10 ( *.pem *.der *.csr );;Requête Netscape ( *.spkac *.spc );;</translation>
    </message>
    <message>
      <source>Import Request</source>
      <translation>Importer une requête</translation>
    </message>
    <message>
      <source>Certificates ( *.pem *.der *.crt *.cer );;</source>
      <translation>Certificats ( *.pem *.der *.crt *.cer );;</translation>
    </message>
    <message>
      <source>Import X.509 Certificate</source>
      <translation>Importer un certificat X.509</translation>
    </message>
    <message>
      <source>PKCS#7 data ( *.p7s *.p7m *.p7b );;</source>
      <translation>Données PKCS#7 ( *.p7s *.p7m *.p7b );;</translation>
    </message>
    <message>
      <source>Import PKCS#7 Certificates</source>
      <translation>Importer un certificat PKCS#7</translation>
    </message>
    <message>
      <source>PKCS#12 Certificates ( *.p12 *.pfx );;</source>
      <translation>Certificats PKCS#12 ( *.p12 *.pfx );;</translation>
    </message>
    <message>
      <source>Import PKCS#12 Private Certificate</source>
      <translation>Importer un certificat privé PKCS#12</translation>
    </message>
    <message>
      <source>XCA templates ( *.xca );;</source>
      <translation>Modèles XCA ( *.xca );;</translation>
    </message>
    <message>
      <source>Import XCA Templates</source>
      <translation>Importer des modèles XCA</translation>
    </message>
    <message>
      <source>Revocation lists ( *.pem *.der *.crl );;</source>
      <translation>Listes de révocation ( *.pem *.der *.crl );;</translation>
    </message>
    <message>
      <source>Import Certificate Revocation List</source>
      <translation>Importer une liste de révocation de certificats</translation>
    </message>
    <message>
      <source>XCA Databases ( *.xdb );;</source>
      <translation>Bases de données XCA ( *.xdb );;</translation>
    </message>
    <message>
      <source>Open XCA Database</source>
      <translation>Ouvrir une Base de Données XCA</translation>
    </message>
    <message>
      <source>PKCS#11 library ( *.dll );;</source>
      <translation>Bibliothèque PKCS#11 ( *.dll );;</translation>
    </message>
    <message>
      <source>PKCS#11 library ( *.dylib *.so );;</source>
      <translation>Bibliothèque PKCS#11 ( *.dylib *.so );;</translation>
    </message>
    <message>
      <source>PKCS#11 library ( *.so );;</source>
      <translation>Bibliothèque PKCS#11 ( *.so );;</translation>
    </message>
    <message>
      <source>Open PKCS#11 shared library</source>
      <translation>Ouvrir une bibliothèque partagée PKCS#11</translation>
    </message>
    <message>
      <source>PEM files ( *.pem );;</source>
      <translation>Fichiers PEM ( *.pem );;</translation>
    </message>
    <message>
      <source>Load PEM encoded file</source>
      <translation>Charger un fichier encodé en PEM</translation>
    </message>
    <message>
      <source>Please enter the PIN on the PinPad</source>
      <translation>Saisir le NIP sur le pavé prévu à cet effet SVP</translation>
    </message>
    <message>
      <source>Please enter the SO PIN (PUK) of the token %1</source>
      <translation>SVP saisir le PUK du jeton '%1'</translation>
    </message>
    <message>
      <source>Please enter the PIN of the token %1</source>
      <translation>SVP saisir le NIP du jeton '%1'</translation>
    </message>
    <message>
      <source>No Security token found</source>
      <translation>Aucun jeton de sécurité trouvé</translation>
    </message>
    <message>
      <source>Select</source>
      <translation>Sélectionner</translation>
    </message>
    <message>
      <source>Please enter the new SO PIN (PUK) for the token: '%1'</source>
      <translation>SVP saisir le nouveau PUK pour le jeton: '%1'</translation>
    </message>
    <message>
      <source>Please enter the new PIN for the token: '%1'</source>
      <translation>SVP saisir le nouveau NIP pour le jeton: '%1'</translation>
    </message>
    <message>
      <source>Required PIN size: %1 - %2</source>
      <translation>Taille du NIP requise: %1 - %2</translation>
    </message>
    <message>
      <source>Failed to open PKCS11 library: %1</source>
      <translation>L'ouverture de la bibliothèque PKCS#11 '%1' a échoué</translation>
    </message>
    <message>
      <source>PKCS#11 function '%1' failed: %2</source>
      <translation>La fonction PKCS#11 '%1' a échoué: %2</translation>
    </message>
    <message>
      <source>PKCS#11 function '%1' failed: %2
In library %3
%4</source>
      <translation>La fonction PKCS#11 '%1' a échoué: %2
Dans la bibliothèque %3
%4</translation>
    </message>
    <message>
      <source>Invalid</source>
      <translation>Invalide</translation>
    </message>
    <message>
      <source>%1 is shorter than %2 bytes: '%3'</source>
      <translation>%1 est plus court que %2 octets: '%3'</translation>
    </message>
    <message>
      <source>%1 is longer than %2 bytes: '%3'</source>
      <translation>%1 est plus long que %2 octets: '%3'</translation>
    </message>
    <message>
      <source>String '%1' for '%2' contains invalid characters</source>
      <translation>La chaîne '%1' pour '%2' contient des caractères invalides</translation>
    </message>
  </context>
  <context>
    <name>ReqTreeView</name>
    <message>
      <source>Sign</source>
      <translation>Signer</translation>
    </message>
    <message>
      <source>Similar Request</source>
      <translation>Requête similaire</translation>
    </message>
  </context>
  <context>
    <name>RevocationList</name>
    <message>
      <source>Manage revocations</source>
      <translation>Gérer les révocations</translation>
    </message>
    <message>
      <source>Add</source>
      <translation>Ajouter</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Enlever</translation>
    </message>
    <message>
      <source>No.</source>
      <translation>No.</translation>
    </message>
    <message>
      <source>Serial</source>
      <translation>Numéro de série</translation>
    </message>
    <message>
      <source>Revocation</source>
      <translation>Révocation</translation>
    </message>
    <message>
      <source>Reason</source>
      <translation>Raison</translation>
    </message>
    <message>
      <source>Invalidation</source>
      <translation>Invalidation</translation>
    </message>
    <message>
      <source>Generate CRL</source>
      <translation>Générer la liste de révocation</translation>
    </message>
  </context>
  <context>
    <name>Revoke</name>
    <message>
      <source>Certificate revocation</source>
      <translation>Révocation du certificat</translation>
    </message>
    <message>
      <source>Revocation details</source>
      <translation>Détails de la révocation</translation>
    </message>
    <message>
      <source>Revocation reason</source>
      <translation>Raison de la révocation</translation>
    </message>
    <message>
      <source>Local time</source>
      <translation>Heure locale</translation>
    </message>
    <message>
      <source>Invalid since</source>
      <translation>Non-valide depuis</translation>
    </message>
    <message>
      <source>Serial</source>
      <translation>Numéro de série</translation>
    </message>
  </context>
  <context>
    <name>SearchPkcs11</name>
    <message>
      <source>Dialog</source>
      <translation>Dialogue</translation>
    </message>
    <message>
      <source>Directory</source>
      <translation>Dossier</translation>
    </message>
    <message>
      <source>...</source>
      <translation>...</translation>
    </message>
    <message>
      <source>Include subdirectories</source>
      <translation>include les sous-dossiers</translation>
    </message>
    <message>
      <source>Search</source>
      <translation>Chercher</translation>
    </message>
    <message>
      <source>The following files are possible PKCS#11 libraries</source>
      <translation>Les fichiers suivants sont susceptibles d'être des bibliothèques PKCS#11</translation>
    </message>
  </context>
  <context>
    <name>SelectToken</name>
    <message>
      <source>Select Token</source>
      <translation>Sélectionner un jeton</translation>
    </message>
    <message>
      <source>Security token</source>
      <translation>Jeton de sécurité</translation>
    </message>
    <message>
      <source>Please select the security token</source>
      <translation>SVP sélectionner le jeton de sécurité</translation>
    </message>
  </context>
  <context>
    <name>TempTreeView</name>
    <message>
      <source>Duplicate</source>
      <translation>Dupliquer</translation>
    </message>
    <message>
      <source>Create certificate</source>
      <translation>Créer un certificat</translation>
    </message>
    <message>
      <source>Create request</source>
      <translation>Créer une requête</translation>
    </message>
    <message>
      <source>copy</source>
      <translation>copier</translation>
    </message>
  </context>
  <context>
    <name>TrustState</name>
    <message>
      <source>Certificate trust</source>
      <translation>Niveau de confiance au certificat</translation>
    </message>
    <message>
      <source>Trustment</source>
      <translation>Niveau de confiance</translation>
    </message>
    <message>
      <source>&amp;Never trust this certificate</source>
      <translation>&amp;Ne jamais se fier à ce certificat</translation>
    </message>
    <message>
      <source>Only &amp;trust this certificate, if we trust the signer</source>
      <translation>Ne &amp;se fier à ce certificat qui si son signataire est de confiance</translation>
    </message>
    <message>
      <source>&amp;Always trust this certificate</source>
      <translation>&amp;Toujours se fier à ce certificat</translation>
    </message>
  </context>
  <context>
    <name>Validity</name>
    <message>
      <source>yyyy-MM-dd hh:mm</source>
      <translation>yyyy-MM-dd hh:mm</translation>
    </message>
  </context>
  <context>
    <name>X509SuperTreeView</name>
    <message>
      <source>OpenSSL config</source>
      <translation>Configuration OpenSSL</translation>
    </message>
    <message>
      <source>Transform</source>
      <translation>Transformer</translation>
    </message>
    <message>
      <source>Template</source>
      <translation>Modèle</translation>
    </message>
    <message>
      <source>Public key</source>
      <translation>Clé publique</translation>
    </message>
  </context>
  <context>
    <name>XcaTreeView</name>
    <message>
      <source>Subject entries</source>
      <translation>Inscriptions du sujet</translation>
    </message>
    <message>
      <source>X509v3 Extensions</source>
      <translation>Extensions X509v3</translation>
    </message>
    <message>
      <source>Netscape extensions</source>
      <translation>Extensions Netscape</translation>
    </message>
    <message>
      <source>Reset</source>
      <translation>Réinitialisation</translation>
    </message>
    <message>
      <source>Remove Column</source>
      <translation>Enlever la colonne</translation>
    </message>
    <message>
      <source>Details</source>
      <translation>Détails</translation>
    </message>
    <message>
      <source>Columns</source>
      <translation>Colonnes</translation>
    </message>
    <message>
      <source>New</source>
      <translation>Créer</translation>
    </message>
    <message>
      <source>Import</source>
      <translation>Importer</translation>
    </message>
    <message>
      <source>Paste PEM data</source>
      <translation>Coller les données en format PEM</translation>
    </message>
    <message>
      <source>Rename</source>
      <translation>Renommer</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Supprimer</translation>
    </message>
    <message>
      <source>Export</source>
      <translation>Exporter</translation>
    </message>
    <message>
      <source>Clipboard</source>
      <translation>Presse-papier</translation>
    </message>
    <message>
      <source>File</source>
      <translation>Fichier</translation>
    </message>
  </context>
  <context>
    <name>db_base</name>
    <message>
      <source>Bad database item
Name: %1
Type: %2
Size: %3
%4</source>
      <translation>Elément de la base de données corrompu
Nom: %1
Type: %2
Taille: %3
%4</translation>
    </message>
    <message>
      <source>Do you want to delete the item from the database? The bad item may be extracted into a separate file.</source>
      <translation>Voulez-vous détruire l'élement de la base de données? L'élément malformé peut être extrait dans un fichier séparé.</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Supprimer</translation>
    </message>
    <message>
      <source>Delete and extract</source>
      <translation>Détruire et extraire</translation>
    </message>
    <message>
      <source>Continue</source>
      <translation>Continuer</translation>
    </message>
    <message>
      <source>Error opening file: '%1': %2</source>
      <translation>Erreur lors de l'ouverture du fichier: '%1': %2</translation>
    </message>
    <message>
      <source>Internal name</source>
      <translation>Nom interne</translation>
    </message>
    <message>
      <source>No.</source>
      <translation>No.</translation>
    </message>
    <message>
      <source>How to export the %1 selected items</source>
      <translation>Comment exporter les %1 éléments sélectionnés</translation>
    </message>
    <message>
      <source>All in one PEM file</source>
      <translation>Tout dans un seul fichier PEM</translation>
    </message>
    <message>
      <source>Each item in one file</source>
      <translation>Chaque élément dans un fichier</translation>
    </message>
    <message>
      <source>Save %1 items in one file as</source>
      <translation>Sauvegarder %1 éléments dans un seul fichier comme</translation>
    </message>
    <message>
      <source>PEM files ( *.pem );; All files ( * )</source>
      <translation>Fichiers PEM ( *.pem );; Tous les fichiers ( * )</translation>
    </message>
  </context>
  <context>
    <name>db_crl</name>
    <message>
      <source>Signer</source>
      <translation>Signataire</translation>
    </message>
    <message>
      <source>Internal name of the signer</source>
      <translation>Nom interne du signataire</translation>
    </message>
    <message>
      <source>No. revoked</source>
      <translation>Numéro révoqué</translation>
    </message>
    <message>
      <source>Number of revoked certificates</source>
      <translation>Nombre de certificats révoqués</translation>
    </message>
    <message>
      <source>Last update</source>
      <translation>Dernière mise-à-jour</translation>
    </message>
    <message>
      <source>Next update</source>
      <translation>Prochaine mise-à-jour</translation>
    </message>
    <message>
      <source>CRL number</source>
      <translation>Numéro de la liste de révocation</translation>
    </message>
    <message>
      <source>The revocation list already exists in the database as:
'%1'
and so it was not imported</source>
      <translation>Cette liste de révocation figure déjà dans la base de données sous le nom:
'%1'
En conséquence, elle n'a pas été importée</translation>
    </message>
    <message>
      <source>Revocation list export</source>
      <translation>Exportation d'une liste de révocation</translation>
    </message>
    <message>
      <source>CRL ( *.pem *.der *.crl )</source>
      <translation>Listes de révocation ( *.pem *.der *.crl )</translation>
    </message>
    <message>
      <source>There are no CA certificates for CRL generation</source>
      <translation>Il n'y a pas de certificat CA for la génération de la CRL</translation>
    </message>
    <message>
      <source>Select CA certificate</source>
      <translation>Sélectionner un certificat d'autorité</translation>
    </message>
  </context>
  <context>
    <name>db_key</name>
    <message>
      <source>Type</source>
      <translation>Type</translation>
    </message>
    <message>
      <source>Size</source>
      <translation>Taille</translation>
    </message>
    <message>
      <source>EC Group</source>
      <translation>Groupe EC</translation>
    </message>
    <message>
      <source>Use</source>
      <translation>Usage</translation>
    </message>
    <message>
      <source>Password</source>
      <translation>Mot de passe</translation>
    </message>
    <message>
      <source>The key is already in the database as:
'%1'
and is not going to be imported</source>
      <translation>Cette clé figure déjà dans la base de données sous le nom:
'%1'
En conséquence, elle ne va pas être importée</translation>
    </message>
    <message>
      <source>The database already contains the public part of the imported key as
'%1
and will be completed by the new, private part of the key</source>
      <translation>La base de donnée connait déjà la partie publique de la clé importée sous le nom
'%1'
En conséquence, cette dernière sera complétée par la partie privée de la clé importée</translation>
    </message>
    <message>
      <source>Key size too small !</source>
      <translation>Taille de clé trop petite !</translation>
    </message>
    <message>
      <source>You are sure to create a key of the size: %1 ?</source>
      <translation>Etes-vous sûr de couloir créer une clé de taille %1 ?</translation>
    </message>
    <message>
      <source>PEM public</source>
      <translation>clé publique PEM</translation>
    </message>
    <message>
      <source>SSH2 public</source>
      <translation>Clé publique SSH2</translation>
    </message>
    <message>
      <source>PEM private</source>
      <translation>Clé privée PEM</translation>
    </message>
    <message>
      <source>Export keys to Clipboard</source>
      <translation>Exporter les clés vers le presse-papier</translation>
    </message>
    <message>
      <source>Clipboard</source>
      <translation>Presse-papier</translation>
    </message>
    <message>
      <source>Export public key [%1]</source>
      <translation>Exporter la clé publique [%1]</translation>
    </message>
    <message>
      <source>DER public</source>
      <translation>Clé publique DER</translation>
    </message>
    <message>
      <source>DER private</source>
      <translation>Clé privée DER</translation>
    </message>
    <message>
      <source>PEM encryped</source>
      <translation>Encrypté en PEM</translation>
    </message>
    <message>
      <source>PKCS#8 encrypted</source>
      <translation>Encryptée en PKCS#8</translation>
    </message>
    <message>
      <source>Export private key [%1]</source>
      <translation>Exporter la clé privée [%1]</translation>
    </message>
    <message>
      <source>Private Keys ( *.pem *.der *.pk8 );; SSH Public Keys ( *.pub )</source>
      <translation>Clés privées ( *.pem *.der *.pk8 );; Clé publiques SSH ( *.pub )</translation>
    </message>
    <message>
      <source>Tried to change password of a token</source>
      <translation>Tentative de changement de mot de passe d'un jeton de sécurité</translation>
    </message>
  </context>
  <context>
    <name>db_temp</name>
    <message>
      <source>Bad template: %1</source>
      <translation>Mauvais modèle: %1</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Type</translation>
    </message>
    <message>
      <source>Nothing</source>
      <translation>Rien</translation>
    </message>
    <message>
      <source>Preset Template values</source>
      <translation>Initaliser les valeurs du modèle</translation>
    </message>
    <message>
      <source>Save template as</source>
      <translation>Enregistrer le modèle sous</translation>
    </message>
    <message>
      <source>XCA templates ( *.xca );; All files ( * )</source>
      <translation>Modèles XCA ( *.xca);; Tous les fichiers ( * )</translation>
    </message>
  </context>
  <context>
    <name>db_x509</name>
    <message>
      <source>CA</source>
      <translation>CA</translation>
    </message>
    <message>
      <source>reflects the basic Constraints extension</source>
      <translation>reflète l'extension des contraintes basiques</translation>
    </message>
    <message>
      <source>Serial</source>
      <translation>Numéro de série</translation>
    </message>
    <message>
      <source>Start date</source>
      <translation>Date de début</translation>
    </message>
    <message>
      <source>Expiry date</source>
      <translation>Date d'expiration</translation>
    </message>
    <message>
      <source>MD5 fingerprint</source>
      <translation>Empreinte MD5</translation>
    </message>
    <message>
      <source>SHA1 fingerprint</source>
      <translation>Empreinte SHA1</translation>
    </message>
    <message>
      <source>SHA256 fingerprint</source>
      <translation>Empreinte SHA256</translation>
    </message>
    <message>
      <source>Not before</source>
      <translation>Pas avant</translation>
    </message>
    <message>
      <source>Not after</source>
      <translation>Pas après</translation>
    </message>
    <message>
      <source>Trust state</source>
      <translation>État de confiance</translation>
    </message>
    <message>
      <source>Revocation</source>
      <translation>Révocation</translation>
    </message>
    <message>
      <source>CRL Expiration</source>
      <translation>Expiration de la CRL</translation>
    </message>
    <message>
      <source>Plain View</source>
      <translation>Vue à plat</translation>
    </message>
    <message>
      <source>Tree View</source>
      <translation>Vue arborescente</translation>
    </message>
    <message>
      <source>The certificate already exists in the database as:
'%1'
and so it was not imported</source>
      <translation>Ce certificat figure déjà dans la base de données sous le nom:
'%1'
En conséquence, il n'a pas été importé</translation>
    </message>
    <message>
      <source>Invalid public key</source>
      <translation>Clé publique invalide</translation>
    </message>
    <message>
      <source>Please enter the new hexadecimal secret number for the QA process.</source>
      <translation>SVP saisir le nouveau nombre secret hexadécimal pour le processus QA.</translation>
    </message>
    <message>
      <source>The QA process has been terminated by the user.</source>
      <translation>Le processus QA a été arrêté par l'utilisateur.</translation>
    </message>
    <message>
      <source>The key you selected for signing is not a private one.</source>
      <translation>La clé sélectionnée pour la signature n'est pas une clé privée.</translation>
    </message>
    <message>
      <source>Store the certificate to the key on the token '%1 (#%2)' ?</source>
      <translation>Enregistrer le certificate avec la clé sur le jeton '%1 (#%2)' ?</translation>
    </message>
    <message>
      <source>PEM chain</source>
      <translation>Chaîne en PEM</translation>
    </message>
    <message>
      <source>PKCS#7 chain</source>
      <translation>Chaîne PKCS#7</translation>
    </message>
    <message>
      <source>PKCS#12 chain</source>
      <translation>Chaîne PKCS#12</translation>
    </message>
    <message>
      <source>PKCS#7 trusted</source>
      <translation>Certificats de confiance PKCS#7</translation>
    </message>
    <message>
      <source>PKCS#7 all</source>
      <translation>Tous les certificats en PKCS#7</translation>
    </message>
    <message>
      <source>PEM + key</source>
      <translation>PEM + clé</translation>
    </message>
    <message>
      <source>PEM trusted</source>
      <translation>Certificats de confiance en PEM</translation>
    </message>
    <message>
      <source>PEM all</source>
      <translation>Tous les certificats en PEM</translation>
    </message>
    <message>
      <source>Certificate Index file</source>
      <translation>Fichier d'index des certificats</translation>
    </message>
    <message>
      <source>Certificate export</source>
      <translation>Exportation de certificat</translation>
    </message>
    <message>
      <source>X509 Certificates ( *.pem *.cer *.crt *.p12 *.p7b )</source>
      <translation>Certificats X509 ( *.pem *.cer *.crt *.p12 *.p7b)</translation>
    </message>
    <message>
      <source>There was no key found for the Certificate: '%1'</source>
      <translation>Aucune clé n'a été trouvée pour le Certificat. '%1'</translation>
    </message>
    <message>
      <source>Not possible for a token key: '%1'</source>
      <translation>Impossible pour une clé d'un jeton de sécurité: '%1'</translation>
    </message>
    <message>
      <source>Not possible for the token-key Certificate '%1'</source>
      <translation>Impossible pour le certificat d'une clé d'un jeton de sécurité: '%1'</translation>
    </message>
    <message>
      <source> days</source>
      <translation> jours</translation>
    </message>
  </context>
  <context>
    <name>db_x509name</name>
    <message>
      <source>Subject</source>
      <translation>Sujet</translation>
    </message>
    <message>
      <source>Complete distinguished name</source>
      <translation>Nom distinctif complet</translation>
    </message>
    <message>
      <source>Subject hash</source>
      <translation>Hachage du sujet</translation>
    </message>
    <message>
      <source>Hash to lookup certs in directories</source>
      <translation>Hachage de recherche dans un dossier</translation>
    </message>
  </context>
  <context>
    <name>db_x509req</name>
    <message>
      <source>Signed</source>
      <translation>Signé</translation>
    </message>
    <message>
      <source>whether the request is already signed or not</source>
      <translation>si la requête a déjà été signée ou non</translation>
    </message>
    <message>
      <source>Unstructured name</source>
      <translation>Nom non-structuré</translation>
    </message>
    <message>
      <source>Challenge password</source>
      <translation>Mot de passe challenge</translation>
    </message>
    <message>
      <source>The certificate signing request already exists in the database as
'%1'
and thus was not stored</source>
      <translation>Cette requête de signature figure déjà dans la base de données sous le nom
'%1'
En conséquence, elle n'a pas été chargée</translation>
    </message>
    <message>
      <source>Certificate request export</source>
      <translation>Exporter la requête de signature</translation>
    </message>
    <message>
      <source>Certificate request ( *.pem *.der *.csr )</source>
      <translation>Requête de signature de certificat ( *.pem *.der *.csr )</translation>
    </message>
  </context>
  <context>
    <name>db_x509super</name>
    <message>
      <source>Key name</source>
      <translation>Nom de la clé</translation>
    </message>
    <message>
      <source>Internal name of the key</source>
      <translation>Nom interne de la clé</translation>
    </message>
    <message>
      <source>Signature algorithm</source>
      <translation>Algorithme de signature</translation>
    </message>
    <message>
      <source>Save as OpenSSL config</source>
      <translation>Enregistrer en format de configuration OpenSSL</translation>
    </message>
    <message>
      <source>Config files ( *.conf *.cnf);; All files ( * )</source>
      <translation>Fichiers de configuration (*.conf *.cnf);; Tous les fichiers ( * )</translation>
    </message>
    <message>
      <source>The following extensions were not ported into the template</source>
      <translation>Les extensions suivantes n'ont pas été enregistrées dans le modèle</translation>
    </message>
  </context>
  <context>
    <name>kvView</name>
    <message>
      <source>Type</source>
      <translation>Type</translation>
    </message>
    <message>
      <source>Content</source>
      <translation>Contenu</translation>
    </message>
  </context>
  <context>
    <name>pass_info</name>
    <message>
      <source>Password</source>
      <translation>Mot de passe</translation>
    </message>
    <message>
      <source>PIN</source>
      <translation>NIP</translation>
    </message>
  </context>
  <context>
    <name>pki_base</name>
    <message>
      <source>Error opening file: '%1': %2</source>
      <translation>Erreur à l'ouverture du fichier '%1': %2</translation>
    </message>
    <message>
      <source>Error writing to file: '%1': %2</source>
      <translation>Erreur à l'écriture du fichier '%1': %2</translation>
    </message>
    <message>
      <source>Error: </source>
      <translation>Erreur: </translation>
    </message>
    <message>
      <source>Internal error: Unexpected message: %1 %2</source>
      <translation>Erreur interne: message inattendu: %1 %2</translation>
    </message>
  </context>
  <context>
    <name>pki_crl</name>
    <message>
      <source>Successfully imported the revocation list '%1'</source>
      <translation>La liste de révocation '%1' a été importée avec succès</translation>
    </message>
    <message>
      <source>Delete the revocation list '%1'?</source>
      <translation>Détruire la liste de révocation '%1' ?</translation>
    </message>
    <message>
      <source>Successfully created the revocation list '%1'</source>
      <translation>La liste de révocation '%1' a été créée avec succès</translation>
    </message>
    <message>
      <source>Delete the %1 revocation lists: %2?</source>
      <translation>Détruire les %1 listes de révocation: %2 ?</translation>
    </message>
    <message>
      <source>Unable to load the revocation list in file %1. Tried PEM and DER formatted CRL.</source>
      <translation>Impossible de charger le liste de révocation du fichier %1. Les formats PEM et DER ont été essayés.</translation>
    </message>
    <message>
      <source>No issuer given</source>
      <translation>Aucun signataire spécifié</translation>
    </message>
    <message>
      <source>Wrong Size %1</source>
      <translation>Taille fausse %1</translation>
    </message>
    <message>
      <source>unknown</source>
      <translation>inconnu</translation>
    </message>
  </context>
  <context>
    <name>pki_evp</name>
    <message>
      <source>Failed to decrypt the key (bad password) </source>
      <translation>Le décryptage de la clé a échoué (mauvais mot de passe). </translation>
    </message>
    <message>
      <source>Please enter the password to decrypt the private key.</source>
      <translation>SVP saisir le mot de passe pour décrypter la clé privée</translation>
    </message>
    <message>
      <source>Please enter the password to decrypt the private key from file:
%1</source>
      <translation>SVP saisir le mot de passe pour décrypter la clé privée du fichier:
%1</translation>
    </message>
    <message>
      <source>Unable to load the private key in file %1. Tried PEM and DER private, public, PKCS#8 key types and SSH2 format.</source>
      <translation>Impossible de charger la clé privée du fichier %1. Les formats PEM et DER ainsi que les types de clés privé, public, PKCS#8 et le format SSH2 ont été essayés.</translation>
    </message>
    <message>
      <source>Ignoring unsupported private key</source>
      <translation>Clé privée non-supportée: ignorée</translation>
    </message>
    <message>
      <source>Please enter the password to decrypt the private key: '%1'</source>
      <translation>SVP saisir le mot de passe pour décrypter la clé privée: '%1'</translation>
    </message>
    <message>
      <source>Password input aborted</source>
      <translation>Saisie du mot de passe abandonnée</translation>
    </message>
    <message>
      <source>Please enter the database password for decrypting the key '%1'</source>
      <translation>SVP saisir le mot de basse de la base de données pour décrypter la clé '%1'</translation>
    </message>
    <message>
      <source>Please enter the password to protect the private key: '%1'</source>
      <translation>SVP saisir le mot de pass pour protéger la clé privée: '%1'</translation>
    </message>
    <message>
      <source>Please enter the database password for encrypting the key</source>
      <translation>SVP saisir le mot de passe de la base de données pour encrypter la clé</translation>
    </message>
    <message>
      <source>Please enter the password protecting the PKCS#8 key '%1'</source>
      <translation>SVP saisir le mot de passe protégeant la clé en PKCS#8 '%1'</translation>
    </message>
    <message>
      <source>Please enter the export password for the private key '%1'</source>
      <translation>SVP saisir le mot de passe d'exportation pour la clé privée '%1'</translation>
    </message>
  </context>
  <context>
    <name>pki_key</name>
    <message>
      <source>Successfully imported the %1 public key '%2'</source>
      <translation>Les %1 clés publiques '%2' ont été importées avec succès</translation>
    </message>
    <message>
      <source>Delete the %1 public key '%2'?</source>
      <translation>Détruire les %1 clés publiques '%2' ?</translation>
    </message>
    <message>
      <source>Successfully imported the %1 private key '%2'</source>
      <translation>Les %1 clés privées '%2' ont été importées avec succès</translation>
    </message>
    <message>
      <source>Delete the %1 private key '%2'?</source>
      <translation>Détruire les %1 clés privées '%2' ?</translation>
    </message>
    <message>
      <source>Successfully created the %1 private key '%2'</source>
      <translation>Les %1 clés privées '%2' ont été créées avec succès</translation>
    </message>
    <message>
      <source>Delete the %1 keys: %2?</source>
      <translation>Détruire les %1 clés: %2?</translation>
    </message>
    <message>
      <source>Public key</source>
      <translation>Clé publique</translation>
    </message>
    <message>
      <source>Common</source>
      <translation>Global</translation>
    </message>
    <message>
      <source>Private</source>
      <translation>Privé</translation>
    </message>
    <message>
      <source>Bogus</source>
      <translation>Erroné</translation>
    </message>
    <message>
      <source>PIN</source>
      <translation>NIP</translation>
    </message>
    <message>
      <source>No password</source>
      <translation>Pas de mot de passe</translation>
    </message>
    <message>
      <source>Invalid SSH2 public key</source>
      <translation>Clé publique SSH2 invalide</translation>
    </message>
    <message>
      <source>Failed writing to %1</source>
      <translation>Echec d'écriture sur %1</translation>
    </message>
  </context>
  <context>
    <name>pki_multi</name>
    <message>
      <source>No known PEM encoded items found</source>
      <translation>Aucun objet PEM connu n'a été trouvé</translation>
    </message>
  </context>
  <context>
    <name>pki_pkcs12</name>
    <message>
      <source>Please enter the password to decrypt the PKCS#12 file:
%1</source>
      <translation>SVP saisir le mot de passe pour décrypter le fichier en PKCS#12:
%1</translation>
    </message>
    <message>
      <source>Unable to load the PKCS#12 (pfx) file %1.</source>
      <translation>Impossible de charger le fichier en PKCS#12 (pfx) %1.</translation>
    </message>
    <message>
      <source>The supplied password was wrong (%1)</source>
      <translation>Le mot de passe renseigné était faux (%1)</translation>
    </message>
    <message>
      <source>Please enter the password to encrypt the PKCS#12 file</source>
      <translation>SVP saisir le mot de passe pour encrypter le fichier en PKCS#12</translation>
    </message>
    <message>
      <source>No key or no Cert and no pkcs12</source>
      <translation>Pas de clé ou pas de certificat et pas en PKCS#12</translation>
    </message>
  </context>
  <context>
    <name>pki_pkcs7</name>
    <message>
      <source>Unable to load the PKCS#7 file %1. Tried PEM and DER format.</source>
      <translation>Impossible de charger le fichier en PKCS#7 %1. Les formats PEM et DER ont été essayés.</translation>
    </message>
  </context>
  <context>
    <name>pki_scard</name>
    <message>
      <source>Successfully imported the token key '%1'</source>
      <translation>La clé du jeton '%1' a été importée avec succès</translation>
    </message>
    <message>
      <source>Delete the token key '%1'?</source>
      <translation>Détruire la clé du jeton '%1' ?</translation>
    </message>
    <message>
      <source>Successfully created the token key '%1'</source>
      <translation>La clé du jeton '%1' a été créée avec succès</translation>
    </message>
    <message>
      <source>Delete the %1 keys: %2?</source>
      <translation>Détruire les %1 clés: '%2' ?</translation>
    </message>
    <message>
      <source>Delete the private key '%1' from the token '%2 (#%3)' ?</source>
      <translation>Détruire la clé privée '%1' sur le jeton '%2 (#%3)' ?</translation>
    </message>
    <message>
      <source>This Key is already on the token</source>
      <translation>Cette clé est déjà sur le jeton</translation>
    </message>
    <message>
      <source>PIN input aborted</source>
      <translation>Saisie du NIP abandonnée</translation>
    </message>
    <message>
      <source>Unable to find copied key on the token</source>
      <translation>Impossible de trouver la clé copiée sur le jeton</translation>
    </message>
    <message>
      <source>Please insert card: %1 %2 [%3] with Serial: %4</source>
      <translation>SVP insérer la carte %1 %2 [%3] avec le numéro de série: %4</translation>
    </message>
    <message>
      <source>Public Key mismatch. Please re-import card</source>
      <translation>La clé publique ne correspond pas. Veuillez re-importer la carte</translation>
    </message>
    <message>
      <source>Illegal Key generation method</source>
      <translation>Méthode de génération de clé illégale</translation>
    </message>
    <message>
      <source>Unable to find generated key on card</source>
      <translation>Impossible de trouver la clé générée sur la carte</translation>
    </message>
    <message>
      <source>Ignoring unsupported token key</source>
      <translation>La clé non-supportée sur le jeton est ignorée</translation>
    </message>
    <message>
      <source>Wrong Size %1</source>
      <translation>Taille fausse %1</translation>
    </message>
    <message>
      <source>Token %1</source>
      <translation>Jeton %1</translation>
    </message>
    <message>
      <source>Failed to find the key on the token</source>
      <translation>Impossible de trouver la clé sur le jeton</translation>
    </message>
    <message>
      <source>Invalid Pin for the token</source>
      <translation>NIP invalide pour le jeton</translation>
    </message>
    <message>
      <source>Failed to initialize the key on the token</source>
      <translation>Impossible d'initialiser la clé sur le jeton</translation>
    </message>
  </context>
  <context>
    <name>pki_temp</name>
    <message>
      <source>Successfully imported the XCA template '%1'</source>
      <translation>Le modèle XCA '%1' a été importé avec succès</translation>
    </message>
    <message>
      <source>Delete the XCA template '%1'?</source>
      <translation>Détruire le modèle XCA '%1' ?</translation>
    </message>
    <message>
      <source>Successfully created the XCA template '%1'</source>
      <translation>Le modèle XCA '%1' a été créé avec succès</translation>
    </message>
    <message>
      <source>Delete the %1 XCA templates: %2?</source>
      <translation>Détruire les %1 modèles XCA: %2 ?</translation>
    </message>
    <message>
      <source>Wrong Size %1</source>
      <translation>Taille fausse %1</translation>
    </message>
    <message>
      <source>Template file content error (too small)</source>
      <translation>erreur de contenu du fichier de modèle (trop petit)</translation>
    </message>
    <message>
      <source>Template file content error (bad size)</source>
      <translation>erreur de contenu du fichier de modèle (mauvaise taille)</translation>
    </message>
    <message>
      <source>Template file content error (too small): %1</source>
      <translation>erreur de contenu du fichier de modèle (trop petit): %1</translation>
    </message>
    <message>
      <source>Not a PEM encoded XCA Template</source>
      <translation>Ce n'est pas un modèle XCA encodé en PEM</translation>
    </message>
    <message>
      <source>Not an XCA Template, but '%1'</source>
      <translation>Ce n'est pas un modèle XCA, mais '%1'</translation>
    </message>
  </context>
  <context>
    <name>pki_x509</name>
    <message>
      <source>Successfully imported the certificate '%1'</source>
      <translation>Le certificat '%1' a été importée avec succès</translation>
    </message>
    <message>
      <source>Delete the certificate '%1'?</source>
      <translation>Détruire le certificat '%1' ?</translation>
    </message>
    <message>
      <source>Successfully created the certificate '%1'</source>
      <translation>Le certificat '%1' a été créé avec succès</translation>
    </message>
    <message>
      <source>Delete the %1 certificates: %2?</source>
      <translation>Détruire les %1 certificats: '%2' ?</translation>
    </message>
    <message>
      <source>Unable to load the certificate in file %1. Tried PEM and DER certificate.</source>
      <translation>Impossible de charge le certificat du fichier %1. Les formats PEM et DER ont été essayés.</translation>
    </message>
    <message>
      <source>This certificate is already on the security token</source>
      <translation>Ce certificat est déjà dans le jeton de sécurité</translation>
    </message>
    <message>
      <source>Delete the certificate '%1' from the token '%2 (#%3)'?</source>
      <translation>Détruire le certificat '%1' sur le jeton '%2 (#%3)' ?</translation>
    </message>
    <message>
      <source>There is no key for signing !</source>
      <translation>Il n'y a pas de clé pour signer !</translation>
    </message>
    <message>
      <source>Wrong Size %1</source>
      <translation>Taille fausse %1</translation>
    </message>
    <message>
      <source>Not trusted</source>
      <translation>Pas sûr</translation>
    </message>
    <message>
      <source>Trust inherited</source>
      <translation>Confiance héritée</translation>
    </message>
    <message>
      <source>Always Trusted</source>
      <translation>Sûr</translation>
    </message>
    <message>
      <source>No</source>
      <translation>Non</translation>
    </message>
    <message>
      <source>Yes</source>
      <translation>Oui</translation>
    </message>
  </context>
  <context>
    <name>pki_x509req</name>
    <message>
      <source>Signing key not valid (public key)</source>
      <translation>La clé de signature n'est pas valide (c'est une clé publique)</translation>
    </message>
    <message>
      <source>Successfully imported the %1 certificate request '%2'</source>
      <translation>Les %1 requêtes de signature '%2' ont été importées avec succès</translation>
    </message>
    <message>
      <source>Delete the %1 certificate request '%2'?</source>
      <translation>Détruire les %1 requêtes de signature '%2' ?</translation>
    </message>
    <message>
      <source>Successfully created the %1 certificate request '%2'</source>
      <translation>Les %1 requêtes de signature '%2' ont été créées avec succès</translation>
    </message>
    <message>
      <source>Delete the %1 certificate requests: %2?</source>
      <translation>Détruire les %1 requêtes de signature '%2' ?</translation>
    </message>
    <message>
      <source>Unable to load the certificate request in file %1. Tried PEM, DER and SPKAC format.</source>
      <translation>Impossible de charger la requête de signature du fichier %1. Les formats PEM, DER et SPKAC ont été essayés.</translation>
    </message>
    <message>
      <source>Signed</source>
      <translation>Signé</translation>
    </message>
    <message>
      <source>Unhandled</source>
      <translation>Non-géré</translation>
    </message>
    <message>
      <source>Wrong Size %1</source>
      <translation>Taille fausse %1</translation>
    </message>
  </context>
  <context>
    <name>v3ext</name>
    <message>
      <source>Add</source>
      <translation>Ajouter</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>Détruire</translation>
    </message>
    <message>
      <source>Apply</source>
      <translation>Appliquer</translation>
    </message>
    <message>
      <source>Validate</source>
      <translation>Valider</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Annuler</translation>
    </message>
    <message>
      <source>An email address or 'copy'</source>
      <translation>Une adresse de courriel ou 'copy'</translation>
    </message>
    <message>
      <source>An email address</source>
      <translation>Une adresse de courriel</translation>
    </message>
    <message>
      <source>a registered ID: OBJECT IDENTIFIER</source>
      <translation>un IDentifiant enregistré: IDENTIFICATEUR.OBJET</translation>
    </message>
    <message>
      <source>a uniform resource indicator</source>
      <translation>un indicateur uniforme de ressource</translation>
    </message>
    <message>
      <source>a DNS domain name</source>
      <translation>u nom de domaine DNS</translation>
    </message>
    <message>
      <source>an IP address</source>
      <translation>une adresse IP</translation>
    </message>
    <message>
      <source>Syntax: &lt;OID&gt;;TYPE:text like '1.2.3.4:UTF8:name'</source>
      <translation>Syntaxe: &lt;OID&gt;;TYPE:texte comme '1.2.3.4:UTF8:nom'</translation>
    </message>
    <message>
      <source>No editing. Only 'copy' allowed here</source>
      <translation>Pas de modification possible. Seul 'copy' est permis ici</translation>
    </message>
    <message>
      <source>Validation failed:
'%1'
%2</source>
      <translation>La validation a échoué:
'%1'
%2</translation>
    </message>
    <message>
      <source>Validation successful:
'%1'</source>
      <translation>La validation a été effectuée avec succès:
'%1'</translation>
    </message>
  </context>
</TS>
